"use strict";

console.debug('\n \n \n');
console.debug('defineProperties');
console.debug('\n \n \n');

/**
 * Objeto literal
 * @class  Object
 */
var Persona = {
    apellido: 'Ronaldo'
};

console.debug('Objeto Persona ', Persona);

//Definir propiedades configurables en un objeto literal
Object.defineProperties(Persona, {
    'nombre': {
        value: 'Cristiano',
        enumerable: true,
        writable: true,
    },
    'edad': {
        writable: true,
        enumerable: false
    },
    'editable': {
        value: false,
        writable: false,
        enumerable: true
    },
    '_nombre': {
        configurable: false,
        enumerable: false,
        //Metodo para poder obtener el valor del metodo saludo
        get: function() {
            return 'Hola ' + this.nombre;
        },
        set: function(Value) {
            return this.nombre = Value;
        }
    },
    'init': {
        value: function() {
            console.debug('Inicializo mi metodo');
        },
        configurable: false,
        enumerable: true,
    }
});
Persona.edad = 29;

console.debug('Objeto Persona con propieedades confiaguradas ', Persona);
console.debug('Atributo _nombre: ', Persona._nombre);
//Llamo al metodo _nombre y le asigno un nuevo valor
Persona._nombre = 'Jedidias';
console.debug('Atributo saludo: ', Persona._nombre);

console.debug('For in de for');
for (var Sujeto in Persona) {
    console.debug(Sujeto);
}

console.debug('Consulto si tiene la propiedad Edad ', Persona.hasOwnProperty('edad'));
console.debug('Edad ', Persona.edad);

console.debug('Init Persona ', Persona.init());


console.debug('\n \n \n');
console.debug('contructPersona prototype');
console.debug('\n \n \n');

function contructPersona(config) {

    this.name = config.name || '';
    this.surname = config.surname || '';
    this.yeards = config.yeards || '';

}

contructPersona.prototype.saludar = function() {
    return 'Hola ' + this.name;
};

var Maria = new contructPersona({
    name: 'Maria',
    surname: 'Rodriguez',
    yeards: 31
});

console.debug('Nueva persona con el contructor ', Maria);
console.debug('Maria tiene el metodo saludar?: ', Maria.hasOwnProperty('saludar')); //Retorna false
console.debug('Maria saluda: ', Maria.saludar());

//********************************
// Nuevo objeto empleado que hereda de Persona
//********************************
console.debug('\n \n \n');
console.debug('Nuevo objeto empleado que hereda de Persona');
console.debug('\n \n \n');

function Empleado(config) {
    contructPersona.call(this, config);
    this.puesto = config.puesto;
}

// Empleado.prototype = new contructPersona();

var nuevoEmpleado = new Empleado({
    name: 'David',
    surname: 'Mora',
    yeards: 29,
    puesto: 'mama'
});

console.debug('Nuevo empleado: ', nuevoEmpleado);

console.debug('\n \n \n');
console.debug(' Nuevo objeto que extiende de otro objeto una copia');
console.debug('\n \n \n');
//********************************
// Nuevo objeto que extiende de otro objeto una copia
//********************************

var empleado2 = Object.create(nuevoEmpleado, {
    name: {
        value: 'Generico'
    },
    surname: {
        value: 'Nemo'
    },
    yeards: {
        value: 40
    },
    puesto: {
        value: 'Desconocido'
    }
})

console.group('empleado generico');
console.debug('empleado generico: ', empleado2);
console.debug(empleado2);

console.debug('\n \n \n');
console.debug('Prueba con clases');
console.debug('\n \n \n');
console.groupEnd();

function DecoratedUser(ValorPrivado) {

    var valorPrivador = ValorPrivado || 'Valor privado por defecto';
    var caminarState = false;
    var testValue = {};
    Object.defineProperty(testValue, 'testValue', {
        value: 'Soy un valor estatico',
        writable: false, // internalValue podrá cambiar de valor
        configurable: false, // internalValue podrá cambiar de valor pero no ser eliminado del objeto
        enumerable: true // internalValue aparecerá en Object.keys o usando for..in
    });
    Object.freeze(testValue);
    Object.seal(testValue);

    function ClassPersona() {

        Object.defineProperty(this, 'internalValue', {
            value: 'Soy un valor estatico',
            writable: false, // internalValue podrá cambiar de valor
            configurable: false, // internalValue podrá cambiar de valor pero no ser eliminado del objeto
            enumerable: true // internalValue aparecerá en Object.keys o usando for..in
        });
        Object.freeze(this.internalValue);
        this.valorNormnal = 'Soy normal.';

    }

    ClassPersona.prototype.caminar = function() {

        caminarState = caminarState ? false : true;
        return caminarState ? 'Comenzo a caminar' : 'Dejo de caminar';

    };
    ClassPersona.prototype.getCaminarState = function() {
        return caminarState;
    }

    ClassPersona.prototype.getPrivado = function() {
        return valorPrivador;
    }

    return new ClassPersona();

}

var David = new DecoratedUser('Hola Mundo');

console.group('Group 1');

console.debug('David');

David.caminar();
console.debug(David.getCaminarState());
console.debug(David.getPrivado());
console.groupEnd();


var Fabian = new DecoratedUser('Hola que tal.');
console.group('Group 2');
console.debug('Fabian');
console.debug(Fabian);
console.debug('Fabian GetPrivado: ', Fabian.getPrivado());
console.debug('David GetPrivado: ', David.getPrivado());
console.groupEnd();

console.debug('\n \n \n');

console.group('Group 3');

console.debug('DecoratedUser');
console.debug(DecoratedUser().caminar());
console.debug(DecoratedUser().getPrivado());
console.debug(DecoratedUser().internalValue);
console.debug('Valor Normnal: ', DecoratedUser().valorNormnal);
DecoratedUser().valorNormnal = 'He sido sobre escrito';
console.debug('Internal Value: ', DecoratedUser().internalValue);
console.debug('Valor Normnal: ', DecoratedUser().valorNormnal);
console.groupEnd();

console.debug('\n \n \n');

var Maria = new DecoratedUser('Hola que tal Maria.');

console.group('Group 4');

console.debug('Maria');
console.debug(Maria);
console.debug(Maria.internalValue);
console.debug(Maria.valorNormnal);

Maria.valorNormnal = 'Cambie mi valor';
console.debug(Maria.valorNormnal);
console.groupEnd();

console.debug('\n \n \n');
console.debug('Prueba');
console.debug('\n \n \n');

var table1 = [];
for (var i = 0; i < 5; i++) {
    table1[i] = [i + 1, i + 2, i + 3, i + 4, i + 5, i + 6, i + 7];
}

console.groupCollapsed("This is my table");
console.table(table1);
console.groupEnd();

console.group('WeakMap');
var myWeakMap = new WeakMap();
myWeakMap.set({}, 37);
myWeakMap.set(window, 'El valor de window');
myWeakMap.set([], 'El valor nombre');
console.log('myWeakMap');
console.dir(myWeakMap);
console.log('Get: ', myWeakMap.get(window));

console.groupEnd();

console.debug('\n \n \n');
console.debug('Clases');
console.debug('\n \n \n');


(function(Object) {


    /**
     * Metodo que almacena metodos y variables privadas usando como llave la instancia de la funcion
     */
    var cotenedorDeHerencia = (function() {

        var arreglo = {};
        return {
            /**
             * Almaceno el valor en el objeto
             * Recive la instancia de la funcion el nombre del valor a almacenar y su valor
             */
            set: function(Func, Key, Val) {
                // console.log('cotenedorDeHerencia set');

                arreglo[Func] = {};
                arreglo[Func][Key] = Val;
                // console.log( Func );
                // console.log( arreglo[Func] );

            },
            /**
             * Retorna los valores almacenados para una instancia especifica
             */
            get: function(Func) {
                // console.log('cotenedorDeHerencia get');
                // console.info( Func, Key );
                // console.log('Valor que retorna el GET');
                // console.info( arreglo[Func][Key] );
                return arreglo[Func];
            }
        };
    }());

    function baseClase(Nombre, Apellido) {

        //Genero un nuevo objeto protegido en el almacen
        cotenedorDeHerencia.set(this, 'valorHeredado', function() {
            console.log('Este es un metodo protegido');
            return this;
        });

        Object.defineProperty(this, 'name', {
            value: Nombre || 'Valor por defecto de nombre',
            writable: false,
            enumerable: false,
            configurable: false
        });
        this.apellido = Apellido;

        this.metodo = function() {

        };

        this.saludar = function() {
            return 'Hola ' + this.name;
        };

        var _ = cotenedorDeHerencia.get(this);
        console.info('Base Clase');
        console.info(_.valorHeredado.bind(this)().saludar());

    }

    console.group('Herencia');

    function claseQueHereda() {

        //Llamoa los valores almacenados en el metodo de el que se hereda pasando el prototipo;
        // var _ = cotenedorDeHerencia.get(this.__proto__);
        // var valorHeredado = _.valorHeredado.bind(this);

        this.algoMas = function() {
            console.log('algoMas');
            return this;
        }

        this.metodo = function() {
            console.info('metodo claseQueHereda');
            return this;
        }

        console.info('claseQueHereda function');
        // console.info(valorHeredado().algoMas());

    };
    claseQueHereda.prototype = new baseClase('Mora');

    var miBase = new baseClase('David', 'Arroyo');
    miBase.apellido = 'Nuevo apellodo';
    console.log('mi nombre es: ', miBase.apellido);

    var nuevaHerencia = new claseQueHereda();

    console.info(miBase);
    console.info(miBase.saludar());

    // console.log('nuevaHerencia');
    // console.info(nuevaHerencia);
    function ClaseNieta(apellido) {
        this.__proto__.__proto__.apellido = apellido;
        console.info(this.__proto__.__proto__);
    }

    ClaseNieta.prototype = new claseQueHereda('Mora');

    var Nieta = new ClaseNieta('Nieta');
    console.info('Nieta');
    console.info(Nieta);





    console.groupEnd();


    console.debug('\n \n \n');
    console.debug('Clases Call');
    console.debug('\n \n \n');

    console.group('Call');

    function claseCall() {
        this.metodoClaseCall = function() {
            console.info('metodo claseCall');
            return this;
        }
        cotenedorDeHerencia.set(this, 'valorProtegido', 'Algo protegido');
    }

    function heredaCall() {
        claseCall.call(this);
        claseQueHereda.call(this);
        console.info( this.__proto__.constructor.constructor[0] );
        var _ = cotenedorDeHerencia.get( new claseCall() );
        console.log('_: ', _);
        // var valorHeredado = _.valorHeredado.bind(this);
    }

    heredaCall.constructor = [claseCall, claseQueHereda];

    var myHeredaCall = new heredaCall();
    console.info( myHeredaCall );


    console.groupEnd();


}(Object));
