/**
 * Categorias.js
 *
 * @module      :: Model
 * @description :: A representation of a user in the DB.
 *
 */

module.exports = {

    attributes: {

        id : {
            type: "integer",
            required: "true",
            unique: true
        },
        name: {
            type: "string",
            required: true,
            unique: true
        },
        slug: {
            type: "string",
            required: "true",
            unique: true
        },
        description: {
            type: "string"
        },
        parent_id : {
            type: 'null'
        },
        state: {
            type: 'boolean'
        },
        created_at: {
            type: 'date'
        },
        updated_at: {
            type: 'date'
        },
        products: {
            type: 'array'
        }
    }

};
