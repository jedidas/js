/**
 * CommentController
 *
 * @description :: Server-side logic for managing comments
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var getSlug = require('speakingurl');
module.exports = {


    /**
     * `CommentController.create()`
     */
    index: function(req, res) {

        // console.log( req.param('id') );
        Categorias.find(function(err, category) {
            console.log(JSON.stringify(category));
            res.render('categorias/categories', {
                title: 'Lista de categorias',
                data: category
            });
        });

    },

    /**
     * `CommentController.create()`
     */
    indexDetail: function(req, res) {

        var id = req.param('id');

        // console.log( req.param('id') );
        Categorias.find({
            slug: req.param('id')
        }, function(err, category) {
            // console.log(JSON.stringify(category));
            res.json({
                category: id
            });
        });

    },


    /**
     * `CommentController.create()`
     */
    create: function(req, res) {
        console.log('create controller');
        return res.view('categorias/create', {
            title: 'Crear categoria',
            data: 'create() is not implemented yet!'
        });
    },

    store: function(req, res) {

        var params = req.params.all(),
            name = req.param('name'),
            slug = getSlug(req.param('name')),
            description = req.param('description'),
            image = req.param('image');

        Categorias.create({
            name: name,
            slug: slug,
            description: description,
            image: image
        }, function(error, category) {
            if (error) {
                return res.json({
                    state: 'fail',
                    error: error
                });
            }

            return res.json({
                state: 'ok'
            });

        });

    },


    /**
     * `CommentController.destroy()`
     */
    destroy: function(req, res) {
        return res.json({
            todo: 'destroy() is not implemented yet!'
        });
    },


    /**
     * `CommentController.tag()`
     */
    tag: function(req, res) {
        return res.json({
            todo: 'tag() is not implemented yet!'
        });
    },


    /**
     * `CommentController.like()`
     */
    like: function(req, res) {
        return res.json({
            todo: 'like() is not implemented yet!'
        });
    }
};