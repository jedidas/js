
require("babel-polyfill");
require('weakmap');
// require('harmony-collections');

'use strict';



console.log('Practica de ES6 Grunt Js');

let myMap = new Map();

//
myMap.set('name', 'David');
myMap.set('country', 'Costa Rica');
myMap.set('temporal', 'Este valor va a ser borrado');
//Los key del Map son unicos y no se repiten si se agrega un nueva valor con un Key ya existente se sobre escribe solamente
console.log(myMap);

//El atributo get consulta por un valor especifico del objeto
console.log('get: ', myMap.get('name'));

//Atributo size devuelve el numero de elmentos
console.log('size: ', myMap.size);

//Atributo delete remuevo el objeto apartir de su llave
console.log('delete: ', myMap.delete('temporal'));

//Retorna un nuevo map con arreglos que corresponden a la llave valor ingresados en el set
console.log('entries() ', myMap.entries());

//Consulta si existe una llave especifica en el Map y retorna un boleano
console.log('has("key") ', myMap.has("country"));

//Devuelve un objeto con las lleves de los diferentes valores
let mapIterator = myMap.keys();
console.log('keys() ', mapIterator);

//Devuelve un objeto con los valores de las llaves
console.log('values() ', myMap.values());

console.log('Objeto ', myMap);

//Recive 3 parametros y permite iterrar por los valores del Map
console.log('forEach(Value, Index, Map)');
myMap.forEach(function(Value, Index, Map) {

    console.log('Value: ', Value);

    console.log('Index: ', Index);

    console.log('Map: ', Map);

});

myMap.clear();
//Limpia el objeto Map
console.log('clear(): ', myMap);


class MyFirstClass {

    constructor() {

    }

    static metodoEstatico() {
        console.log('mi metodo estatico');
    }

    algo(Value = 'Valor por defecto') {
        return Value;
    }

}

class Persona extends MyFirstClass {
    // this.job;
    constructor(JobsValue = 'Desempleado') {
        super();
        this.job = JobsValue;
        //Se agregan atributos estaticos a la clase
        Object.defineProperty(this, 'elements', {
            value: 'Valor',
            writable: false
        });
        Object.freeze(this.elements);
    }
    getJob() {
        console.log(`Mi trabajo es ${this.job}`);
    }
}

let People = new Persona('Front End');

console.log('static', MyFirstClass.metodoEstatico());

console.log('People:: ', People);
console.log(People.algo('People'));
console.log(People.algo());
People.getJob();




//WeakMap
var ClassConPrivateValues;
{
  let priv = new WeakMap();
  ClassConPrivateValues = class ClassConPrivateValues {
    constructor() {
      priv.set(this, function () {
        console.log(' Metodo privado de la clase ');
      });
    }
    metodo () {
      console.log( priv.get(this)() );
      return 'Metodo publico. Return';
    }
  }
}

var ClaseConMetodosPrivados; {
    //Variables privadas
    let keyAcces = new WeakMap();
    let calcularValor = new WeakMap();
    //Clase
    ClaseConMetodosPrivados = class ClaseConMetodosPrivados {

        constructor(sumarValue = 0) {
            //atributo publico
            this.sumarValue = sumarValue;
            //Atributo privado
            keyAcces.set(this, '123456');
            //metodo privado
            calcularValor.set(this, function() {
                return this.sumarValue;
            }.bind(this));
        }
        sumar(Value = 0) {
            return calcularValor.get(this)() + Value;
        }
    }
};
 var miNuvaClase = new ClassConPrivateValues();
console.log('miNuvaClase: ');
console.log( miNuvaClase.metodo() );

var TestArrowFunction = function() {

    console.log('TestArrowFunction');
    this.methotTest = () => {
        console.log('methotTest');
        return this;
    };
    this.generateAlgo = () => {

        console.log('generateAlgo');
        return this;

    };

};

var miClase = new ClaseConMetodosPrivados(5);
var miClaseDos = new ClaseConMetodosPrivados();


class Operaciones extends ClaseConMetodosPrivados {
    constructor(Value) {
        super(Value);
        Object.defineProperty(this, 'staticValue', {
            value: 'Soy un valor estatico',
            writable: false,
            configurable: false,
            enumerable: true
        });
    }
    multiplicar(multi) {
        return multi;
    }
}

var Myoperaciones = new Operaciones(2);

console.group('Clase con WeakMap');

console.info(miClase.sumar(5));
console.info(miClase.sumar());

console.log('miClaseDos');
console.info(miClaseDos.sumar(4));

console.group('Clase con herencia');

console.log('staticValue Object');
console.info( Myoperaciones.staticValue );
console.info( Myoperaciones.sumar(4) );

console.groupEnd();

console.groupEnd();

var NuevaFunction = new TestArrowFunction();
NuevaFunction.generateAlgo().methotTest();

var Myoperaciones = new Operaciones(2);

console.group('Clase con WeakMap');

console.info(miClase.sumar(5));
console.info(miClase.sumar());

console.log('miClaseDos');
console.info(miClaseDos.sumar(4));

console.group('Clase con herencia');

console.log('staticValue Object');
console.info( Myoperaciones.staticValue );
console.info( Myoperaciones.sumar(4) );

console.groupEnd();

console.groupEnd();
