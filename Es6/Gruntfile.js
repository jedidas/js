module.exports = function(grunt) {

    grunt.initConfig({

        compass: {
            dist: {
                options: {
                    config: './config.rb'
                }
            }
        },

        concat: {
            dist: {
                src: [
                    './web/js/app.js'
                ],
                dest: './.temp-js/app.js',
            }
        },

        uglify: {
            bundle: {
                files: {
                    './web/js/app.min.js': './.temp-js/app.js'
                }
            }
        },
        browserify: {
            dist: {
                options: {
                  transform: [['babelify', {presets: ['es2015']}]]
                },
                files: {
                    './web/js/app.js': ['./app/app.js']
                },
            }
        },

        watch: {
            sass: {
                files: ['./sass/**/*.scss'],
                tasks: ['compass'],
                options: {
                    spawn: false
                }
            },
            js: {
                files: ['./app/**/*.js'],
                tasks: ['browserify', 'concat', 'uglify'],
                options: {
                    spawn: false
                }
            }
        }





    });

    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['watch']);


};
