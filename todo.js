/*-----Array----*/

/*==================================
			Tips
==================================*/
/*
Valores que son negativos o boleaanos FALSE
-------------------------------------------
false
0
""
null
undefined
NaN


1- No se recomiendan usar variables globales, ya que contaminan el Scope global con propiedades que no son propias del windows.
2- Debe evitarse el conflicto con metodos y propiedades, utilizando un NameSpace
NameSpace: Objeto que contiene todos los metodos y  funciones, en si una API
3- Siempre se debe inicializar una variables al principio del documento


buscar Operacion ternaria
variable = primerValor ? variable = segundoValor;
la variable es igual al primer valor y si es FALSE es igual al segundo
*/



/*
slice(start, end); : extrae una parte de un arreglo entre los parametros se definen el origen y el final, si no se aplica el segundo
parametro lo toma desde el primer parametro hasta el final
*/
slice(start, end);

//convertir nodeList a Array
var pars= document.getElementsByTagName('p');
var passArray = Array.prototype.slice.call(pars, 0);

//parte del objeto window, que esta en el scope global
document

//Este metodo permite saber el tipo de elemento que es un NODO, devuelve un valor numerico
elemento.nodeType
element_node: 1
attibute_node: 2


//Estos metodos pertenecen al Document
getElementsByTagName();
getElementById();

/*
Estos perteneces al element y el document
De manera que se pueden llamar a un getElementsByClassName, despues de un getElementById
*/
getElementsByTagName();
getElementsByClassName();



/*
style es parte del Objeto Element
Si no se le pasa ningun valor el devuelve un valor
Ninguna propiedad definida en el CSS afecta los valores que devuelve por el atributo style
El style solo devulve los valores del atributo style
*/
style;



/*
Sirve para saver los valores asignados al elemento en la hoja de estilos
Este metodo devuelve un objeto CCSStyleDeclaration Json con las propíedades y sus valores
No se pueden modificar los valores del CSS

*/
getComputedStyle();



//Esto es lo mismo
var y= document.getElementById("myID").getElementsByClassName("myClass");
//a esto
var x= document.getElementById("myID"); 
var x= y.getElementsByClassName("myClass");


/*Selectores en javascript*/
getElementById();
/*
Selecciona IDs unicos
*/


getElementsByName();
/*
Selecciona al atributo name que solo se le pueden poner a elementos de FORM, Imagenes, Iframes, embet
Devuelde un html collection
Al usar Name crea un elemento global en el window
*/

getElementsByTagName();
/*
Esta definido en el document y el element
Retorna un nodeList Object
Se puede utilizar el * para seleccionar todos los nodos con el comodin
document.getElementsByTagName('*');
*/


getElementsByClassName();
/*
Selecciona las Clases
devuelve una lista de nodos.
Se le pueden pasar varios valores de clases en un solo parametro y el solo selecciona los elementos que posean ambas clases.
*/


/*Selectores de Css, existen 2 metodos*/
querySelectorAll();
/*
Devuelve una lisa de nodos que no es viva, es mas bien como una captura del DOM en el momento
*/

querySelector();
/*
solo devuelve una elemento, en caso de ser clases solo devuelve el primero
*/



/*
selector avanzado
Permite seleccionar los hijos como en un array indicando la posicion del hijo deseado
Ademas permite realizar operaciones matematicas simples
*/
document.querySelector('.nav li: nth-child(2)');




/*Metodos del document*/
createAttribute(name,value)
createComment(value) //crea un comentario
/*
Ej:
var fragment = document.createDocumentFragment();

var contents = fragment.appendChild(document.createElement('blockquote'));
contents = contents.appendChild(document.createElement('p'));
contents.appendChild(document.createTextNode('Always two there are'));

element.appendChild(fragment);
*/
document.documentElement //Hace referencia al html del documento. Una propiedad de acceso directo que se refiere al nodo de elemento raíz del documento, en HTML que este sería el elemento html.

getElementsByTagName(nodeName) //devuelve todos los en un NodeList, que es dinamico vivo y cambiante 
/*
var lists = getElementById('#list');
var items = list.getElementByTagName('li');
Este método también se puede utilizar contextualmente, para obtener una referencia a los elementos que están dentro de un elemento específico, por ejemplo:
*/

createElement(nodeName) //Crea un nodo nuevo
/*
IE7
Internet Explorer en HTML soporta una sintaxis corrompida, que permite un subárbol que se creará mediante el paso marcado serializado a este método, por ejemplo:
document.createElement('<p>Oh dear...</p>');
*/

importNode(externalNode, boolean);
/*
Permite clonar un nodo de un archivo externo ejemplo un IFRAME, atravez del metodo contentDocument
El valor boolean determina si se importa los hijos del nodo o no, default es true

var iframe = document.getElementsByTagName("iframe")[0];
var oldNode = iframe.contentDocument.getElementById("myNode");
var newNode = document.importNode(oldNode, true);
document.getElementById("container").appendChild(newNode);
*/








/*
NODELIST
A pesar de que una lista de nodos se parece a una matriz, no es un conjunto - aunque se puede recorrer y consultar a sus miembros como una matriz, no puede utilizar los métodos de matriz como push o pop en él.

HTMLCollection
Es una interfaz que representa una colección genérica de elementos ( en el orden del documento ) y ofrece métodos y propiedades para recorrer la lista .
*/






/*
Sirve para recorrer el DOM como un arbol de nodos de manera que los textos y espacios en blanco los reconoce como nodos
*/

parentNode //: Padre del elemento 
childNodes //: Hijos del nodo
firstChild, lastChild //: Primer Hijo Ultimo Hijo
nextSibling, previousSibling //: Proximo hermano y el hermano anterior 
nodeType //: Define el tipo de nodo
nodeValue //: Devuelve el valor del nodo si es texto
nodeName //: Para sabe el nombre del nodo devuelve un String con el tipo de nodo

/*
Otro Medoto de recorrer el DOM como un arbol de elementos ignorando espacios en blanco y textos. Devuelde una lista de elementos HTML 
*/

children // : Hijos
firstElementChild, LastElementChild //: Primer Hijo Ultimo Hijo
nextElementSibling, lastElementSibling //: Proximo hermano y el hermano anterior 
childElementCount //: Devuelve la cantidad de hijos del elemeto


//
getAttribute(); //Permite leer el valor de cualquier atributo
setAttribute(); //Permite agregar un atributo a un elemento se pasan 2 parametros, el nombre del atributo y su valor, sobre-escribiendo los atributos existentes
removeAttribute(); // Remueve un atributo
hasAttribute(); //Al preguntar por un atributo devuelve un booleano si existe o no
img.attributes; //Devuelve una lista con todos los atributos de un elemento

/*------------Textos-------------*/
innerHTML //: Propiedad, que permite saber el valor HTML de un nodo o asignar un nuevo valor
outerHTML //: Remplaza todo lo que esta afuera del Nodo
insertAdjacentHTML //: Permite mandar un bloque a un elemento, y posicionarlo en un lugar especifico determinando parametros

textContent //: Inserta textoplano funciona en IE
innerText //: Inserta solo texto plano o todo como texto plano






/*-------------Crear Nodos-------------*/


//Crear elementos de nodo
document.createElement()

//creo un nodo de texto
document.createTextNode();


/*Para clonar nodos existentes
Es un metodo de element,
Se le pasa un parametro boleano
si es FALSE solo clona el contenedor
True clona el nodo con los hijos
*/
cloneNode();


//Crear fragmentos muy util
/*
Crea un fragmento de nodos
Es un contenedor temporal no tinene parent, se mantiene en memoria no tiene un padre
AL agregar hijos son tratados como nodos unicos,
Sirve para agregar multiples nodos a la vez, el contenedor como tal no existe, o no se inserta
*/
document.createDocumentFragment();



/*-------------Agregar Nodos-------------*/

/*
Al user appendChild e insertBefore sobre un nodo existente en vez de crear uno se puede editar el nodo cambiandolo de lugar sin necesidad de borrarlo
*/

/*
Inserta un nodo hijo sobre un nodo padre, aqui se le puede agregar un fragment
Siempre lo inserta al final del nodo padre
*/
NodoPadre.appendChild();


/*
Insertar antes de uno de los nodos
Lleva 2 argumetos el nodo a insertar y el nodo al que se le insertara un nodo hermano antes
Se debe definir el nodo al que se le agregara el nodo
Si el segundo argumento es NULL se comportaria como appendChild();
*/
NodoPadre.insertBefore('nodo a insertar', 'Nodo hermano');




/*-------------Borrar Nodos-------------*/
/*
Se invoca en el padre
Es parte de ELEMENT
*/
NodoPadre.removeChild('Hijo a remover');


/*-------------remplazar Nodos-------------*/
/*
Se invoca en el padre
Es parte de ELEMENT
*/
NodoPadre.replaceChild('Nuevo Nodo','Nodo a remplazar');



/*-------------this-------------*/
/*
this: Se utiliza en POO
Se aplica o 
*/


/*-------------Declarar funciones-------------*/
/*
Toda funcion retorna undefined a menos que se declare el return
*/
//declaracion normal al hacer el Hoisting pasa toda la funcion a la parte mas elevada
function nombre(){}

//declaracion como expresion, al hacer el Hoisting se pasa la variable pero no el cuerpo
var nombre2 = function(){ }

//self invoke auto ejecutable
/*
Al colocarla en parentesis se convierte en una expresion y los parentesis al final estos ejecutan la funcion anonima
*/
(function(){ })();

/*Al utilizar cualquier operador unario se puede ejecutar una funcion anonima*/
+function(){  }();



/*-------------Hoisting -------------*/
/*
Elevasion de una variable;
Al declarar una variable esta se pone en el nivel mas alto del Scope de manera que la variable queda declarada de primero aunque su valor donde se inicializa y se le asigna un valor queda en la misma posicion, se puede llamar a la variable en cualquier momento pero su valor solo se conoceria hasta se llegue a la declaracion de la misma

1 - Las funciones buscan un valor de una variable dentro de su Scope local si se declara una funcion local igual a una funcion global la funcion busca el valor de la variable local.
2 - Para acceder al valor global se debe utilizar el metodo this que en este caso hace referencia al windows
3 - Las funciones que son expresiones pueden ser llamadas antes de ser declaradas
4 - Desde una funcion interna dentro de otra funcion se puede ejecutar los valores globales dentro del scope local de la funcion contenedora
*/




/*-------------Eventos javascript-------------*/
/*
Existen 2 maneras de registrar eventos:
1 - Colocar un atributo onClick en el elemento
2 - 

//Contexto del this en el Event handler
Al ejecutar un metodo Eventpropagatio handler, dicho metodo se ejecuta como un metodo del objeto cuando este es definido de manera que el this haria referencia al elemento que invoco el metodo.



Los Event handler: son un parametro con los eventos que se ejecutan, al ejecutar una funcion o metodo.

//Existen
Event type: nombre del evento o tipo de evento que se ejecuta
Event target: Es el objeto con el cual esta asociado el evento o el objeto que ejecuta dicho evento.
Event handler o Event listener: es la funcion que se ejecuta al disparar el evento. (El addEventListener agrega un handler al objeto)
Event object: Es el parametro por defecto con las propiedades del evento, que se pasa a la funcion en el handler.
Eventpropagatio: Es el evento por el cual el navegador decide el ordenen que se ejecuta y propaga un evento

//keyEvent
Al realizar un keyEvent si existe un focus en un elemento, el keyEvent formara parte del evento del elemento de no haber ningun elemento en focus el evento correspondera al window.

//DOM Event
DOMContentLoaded: se ejecuta cuando el contendo de ua pagina se carga, menos las imagenes.
unload: Se ejecuta cuando se cierra una ventana.
beforeunload: Muestra un mensaje Promp por si quiere cerrar la ventana.

Progressive Enhancement
Unobstrusive

removeEventListener: remueve el listener de un elemento


substring();
indexOf();
*/




/*-------------Funciones CrearNameSpace-------------*/

/*
Las funciones y los valores son lo mismo para javascript, de manera que se puede definir o pasar un valor como una funcion con facilidad

Para crear un nameSpace se encapsula todo el codigo dentro de una funcion.
Tambien podemos defiir un objeto {} y asignarle metodos y propiedades.

Las funciones son objetos.
Los objetos son colecciones de llaves de pares, valor.
Las funciones se le pueden asignar a variables y a objetos, tambien a arreglos.
Se pueden pasar como argumentos.
Las funciones pueden anidarse y definirse una dentro de otra funcion, estas tienen acceso a las variables externas.
Las funciones tienen un link al contexto externo Clowsure.
TODAS LAS FUNCIONES TIENEN 2 PARAMETROS ADICIONAES propios de todas las funciones, this, arguments (arguments: Es un ArrayType, se parece a un array pero no lo es del todo, permite recorrer los argumetos como una matris)

Los parametros son los que se pasan a una funcion y los argumetos es cuando se reciven los parametros.
Si una funcion se le envian menos parametros de los que espera, los parametros que no se envian, serian undefined
*/

/*-------------Funciones Patrones de invocasion-------------*/

/*
Existen 4
1- Function Invocation
2- Function New

Al definir una clase el nombre va en Mayuscula.


*/

/*------------Definiendo this-------------*/

/*
si la funcion se invoca directamente this seria global Object Windows
Si la funcion es invocada como un metodo de una funcion this, seria igual al objeto al que pertenece el metodo.
AL tratar de definir this dentro de una funcion anidada this se pierde de manera que hay que definirla en la funcion wrapper.
*/



/*------------Objetos-------------*/
/*
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects?redirectlocale=en-US&redirectslug=JavaScript%2FReference%2FGlobal_Objects
https://developer.mozilla.org/es/docs/Introducci%C3%B3n_a_JavaScript_orientado_a_objetos
http://www.ecured.cu/index.php/Programaci%C3%B3n_Orientada_a_Objetos#Encapsulamiento

En javascript no existen metodos o clases como tal
Cada objeto en JavaScript es una instancia del objeto Object

*/

/*
Terminologia

Class
Define las características del Objeto. Las clases se deben definir en Mayuscula, para diferenciarlas

Object
Instancia de una clase

Propiedad
Una característica del Objeto, como el color.

Método
Una capacidad del Objeto, como caminar.

Herencia
Una Clase puede heredar características de otra Clase.

Encapsulamiento
Una Clase sólo define las características del Objeto, un Método sólo define cómo se ejecuta el Método.
Es cuando se define un metodo de control para limitar al acceso a las propiedades y metodos de un objeto

Abstracción
La conjunción de herencia compleja, métodos, propiedades de un objeto debe ser capaz de simular un modelo de la realidad.

Polimorfismo
Diferentes Clases podrían definir el mismo método o propiedad.
Usar metodos de otra clase dentro de una clase, sin que este metodo lo poseyera inicialmente

Prototipos

*/


/*Tipos de Datos de Javascript*/

/*
Primitovos y Objetos
Primitivos: numericos, strean, boolean 


Objetos: los objetos son colecciones de key-values


Objetos literales: se definen los metodos y valores dentro del objeto, es mucho mas eficiente que new
*/

var person = {
	firstName : "Name",
	lastName  : "DOe",
	sayHi     : function{
		return "Hi there";
	}
}
//agrega una nueva propiedad a todos las instancias
person.prototype.newMeth = function(){
	//values
}


//agrega una nueva propiedad solo al objeto sin heredarlo a las instancias del mismo
perosn.newMeth2 = function(){
	//values
}


/*
Object factory
Sirve para crear objetos dimamicos
*/

var createPerson = function(firstName,lastName){
	return {
		firstName : firstName,
		lastName  : lastName,
		sayHi     : function{
			return "Hi there";
		}
	}
}


/*
Object.defineProperty
Es demaciado moderno
*/


/*
Constructor
Al usar el contructor se crea un nuevo tipo de dato, se usa cuando se usa mas de un objeto del mismo tipo y si no se usa un literal

*/
var Person = function(firstName,lastName){
	this.firstName = firstName;
	this.lastName = lastName;
}

Person.prototype.sayHello = function(){
	return 'Hi there';
}


Person.prototype.newsMetod = function(){

	//para llamar a un metodo publico del objeto de define this en una variable privada dentro del Scope
	_this = this;
	function subMetodo = function (){
		//de esta manera se mantiene el puntero del this
		_this.sayHello();
	}
}

/*------------JQUERY-------------*/
/*
Selectores
*/
$('').size();//Longitus o numero de elementos
$(' li:odd');
$('#main ').find('li');//Seleccion filtrada es mucho mas eficiente que la seleccion normal '#main li'
$('#main ').find('li').end();//jQuery.end() vuelve al punto anterior dentro de la cadena de filtros
$('#main ').parent();//selecciona el padre del nodo
$('#main ').parents();//selecciona todos los padres del nodo el padre del padre del padre etc
$('#main ').closest('selector');//Detiene el evento del parents de manera que no recorra todos los padres y se detenga en el selector indicado.

$('#main ').append('Nodo');//Agrega un nuevo nodo a un elemento
$('#main ').appendTo('selector');//Es lo mismo que el append

$('#main ').prepend('Some text');//Inserta un nodo de primero o antes de un nodo X
$('#main ').prependTo('Some text');//Inserta un nodo de primero o antes de un nodo X
$('#main li').eq(1);//Estos son los indices aqui solo selecciona el primero

/*Dom travels*/






/*
JSON


getJSON



*/










