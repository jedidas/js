function App() {

    var modulos = {
            factory: {}
        },
        $dependencias = {
            $rootScope: {},
            $scope: (function() {
                return new Object({});
            }()),
            $http: function (object, succes, error) {

                var xhr;
                if(window.XMLHttpRequest) {
                    xhr = new XMLHttpRequest();
                }
                else if(window.ActiveXObject) {
                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
                }

                var config = {
                    method: object.method || 'GET',
                    type: object.type || '',
                    url: object.url || '',
                    data: object.data || null,
                    async: object.async || true
                };
                xhr.open(config.method, config.url, config.async);
                xhr.onreadystatechange = function () {
                    if(xhr.readyState === 4){
                        var status = xhr.status;
                        if( (status >= 200 && status < 300) || (status === 304) ){
                            if(config.type==='json' || config.type==='JSON' || config.type==='Json'){
                                succes( JSON.parse(xhr.responseText) );
                            }else{
                                succes( xhr.responseText );
                            }
                            return xhr.response;
                        }else{
                            error(status, 'error');
                        }
                    }
                };
                xhr.send(config.data);

            }
        },
        $ruteo = {},
        other = '',
        viewsCache = {},
        content = document.querySelector('[data-view]'),
        $rutas = {
            when: function(url, callback) {
                $ruteo[url] = callback;
                return this;
            },
            otherwise: function(value) {
                other = value;
                return this;
            }
        };

    function loadView (url) {

        $dependencias.$http({
            method: 'GET',
            url: url
        }, function(data){
            content.innerHTML = data;
        }, function(error){
            console.log( error );
        });

    }

    function initController() {

        var controllerName = window.location.hash.split('/')[1];
        if (!$ruteo.hasOwnProperty(controllerName)) {
            window.location.hash = '#/' + other;
            return;
        }
        var template = $ruteo[controllerName].templateUrl;
        loadView (template);

        controllerName = $ruteo.hasOwnProperty(controllerName) ? $ruteo[controllerName].controller : $ruteo[other].controller;

        for (var key in modulos.factory) {

            if (key === controllerName) {
                if (Array.isArray(modulos.factory[key][1])) {
                    //modulos.factory[key][0](modulos.factory[key][1][0], modulos.factory[key][1][1]);
                    modulos.factory[key][0].apply(null, modulos.factory[key][1]);
                } else {
                    modulos.factory[key][0].apply(null, modulos.factory[key][1]);
                    //modulos.factory[key][0](modulos.factory[key][1]);
                }
            }

        }

    }

    this.controller = function(nombreController, callback) {

        modulos.factory[nombreController] = [];

        if (typeof callback === 'function') {
            modulos.factory[nombreController].push(callback);
            modulos.factory[nombreController].push({});
        }

        if (Array.isArray(callback)) {

            var inyeccion = [];
            for (var i = 0, count = callback.length; i < count; i++) {
                if (typeof callback[i] === 'string') {
                    if ($dependencias[callback[i]]) {
                        inyeccion.push($dependencias[callback[i]]);
                    }
                } else {
                    if (typeof callback[i] === 'function') {
                        modulos.factory[nombreController].push(callback[i]);
                        modulos.factory[nombreController].push(inyeccion);
                    }
                }
            }
        }
        return this;
    };

    this.route = function(url, objeto) {

        $rutas[url] = objeto;

        return this;
    };

    this.config = function(callback) {
        callback($rutas);
        return this;
    };

    this.init = function() {

        initController();
        return this;

    };

    (function() {
        window.addEventListener("hashchange", initController, false);
    }())

}