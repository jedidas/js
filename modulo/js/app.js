var myApp = new App();
myApp.config(function($url) {

    $url.when('home', {
        controller: 'HomeController',
        templateUrl: 'views/home.html'
    });
    $url.when('proceso', {
        controller: 'ProcesoController',
        templateUrl: 'views/proceso.html'
    });
    $url.when('nuevo', {
        controller: 'NuevoController',
        templateUrl: 'views/nuevo.html'
    });
    $url.otherwise('home');

})
    .controller('HomeController', function() {
        console.log('HomeController');
    })
    .controller('ProcesoController', ['$scope', '$rootScope',
        function($scope, $rootScope) {

            console.log('ProcesoController');
            $scope.nombre = 'David Mora $scope'
            $rootScope.estado = 'Este es un valor global';
            console.log('Prueba de Scope');
            console.log($scope);
        }
    ])
    .controller('NuevoController', ['$scope', '$rootScope', '$http',
        function($scope, $rootScope, $http) {

            console.log('NuevoController');
            console.log($rootScope.estado);

            $http({
                url: 'http://local.apiangular/api/articles',
                type: 'JSON'
            }, function(data) {
                console.log('Api');
                console.log( data );
            }, function(error) {
                console.log('Api error', error);
            });

        }
    ])
    .init();