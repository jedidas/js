(function(window) {
    'use strict';
    window.$App = bootstrap;

    //Valores privados
    var Resources = {
        'constants': {},
        'controller': {},
        'factory': {},
        'mode': null,
        'root': '/',
        'routes': [],
        'dependancy': {},
    },

    api = {
        'factory': function(key, arrayArg) {

        },
        'routes': function(route, controller) {

        },
        'controller': function(controller, handler) {

        },
        'constants': function(key, val) {
            Resources.constants[key] = val();
        },
        'loadDependancy': function(arrayArg) {
            var dependancy = [],
                iter;
            for (iter = 0; iter < arrayArg.length; iter += 1) {
                if (typeof arrayArg[iter] === "string") {
                    //look in modules
                    if (resources.hasOwnProperty(arrayArg[iter])) {
                        dependancy.push(api.loadModule(arrayArg[iter]));
                    } else {
                        //look in factory
                        if (resources.factory.hasOwnProperty(arrayArg[iter])) {
                            dependancy.push(api.loadDependancy(arrayArg[iter]));
                        } else {
                            //look in constants
                            if (resources.constants.hasOwnProperty(arrayArg[iter])) {
                                dependancy.push(api.loadConstant(arrayArg[iter]));
                            } else {
                                //if it is $me scope
                                if (arrayArg[iter] === "$mi") {
                                    dependancy.push({});
                                } else {
                                    console.log("Error: " + arrayArg[iter] + " is not Found in constants and Factories");
                                }
                            }
                        }
                    }
                }
            }
            return dependancy;
        }
    };

    function bootstrap() {
        function contant() {
            var key = arguments[0],
                val = arguments[1];
            console.log(key);
            console.log(val);
        }

        function routes() {
            var key = arguments[0],
                val = arguments[1];
        }

        function controller() {
            var key = arguments[0],
                val = arguments[1];
        }

        function factory() {
            var key = arguments[0],
                val = arguments[1];
            console.log(key);
            console.log(val);
        }

        return {
            contant: contant,
            routes: routes,
            controller: controller,
             
            factory: factory
        }

    }


}(window));