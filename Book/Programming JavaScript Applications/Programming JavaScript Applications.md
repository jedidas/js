# Programming JavaScript Applications


##Diseño del método

Existen varias técnicas en JavaScript para diseñar los métodos del API. JavaScript admite listas de parámetros nombrados, polimorfismo de funciones, encadenamiento de métodos y expresiones lambda.

Usted debe estar familiarizado con todas estas técnicas para que pueda elegir la herramienta adecuada para el trabajo.

Hay algunos principios a tener en cuenta al diseñar sus métodos. Esto tiene que repetir:

- Keep It Simple, Stupid (**KISS**)
- Do One Thing (**DOT**), and do it well
- Don't Repeat Yourself (**DRY**)

## Parámetros con nombre

El número de variables que pasa a una función se llama su **arity**. En general, la función **arity** debe mantenerse pequeña, pero a veces se necesita una amplia gama de parámetros (por ejemplo, para inicializar la configuración de un módulo o crear una nueva instancia de objeto). El problema con un **arity** grande es que cada parámetro debe pasar a la función en el orden correcto, incluso si no son necesarios varios parámetros. Puede ser difícil recordar cuál es el orden requerido, y no tiene sentido requerir un parámetro que no es realmente necesario para que la función haga su trabajo correctamente.

Este ejemplo está diseñado para configurar una nueva cuenta de usuario. Cada cuenta de usuario tiene algunas configuraciones predeterminadas que se respetan a menos que se pase un valor de anulación:

```
var userProto = {
    name: '',
    email: '',
    alias: '',
    showInSearch: true,
    colorScheme: 'light'
};

function createUser(name, email, alias, showInSearch,
    colorScheme) {
    return {
        name: name || userProto.name,
        name: email || userProto.email,
        alias: alias || userProto.alias,
        showInSearch: showInSearch,
        colorScheme: colorScheme || userProto.colorScheme
    };
}

test('User account creation', function() {

    var newUser = createUser('Tonya', '', '', '', 'dark');
    equal(newUser.colorScheme, 'dark', 'colorScheme stored correctly.');
    
});
```

En este caso, la función createUser() toma cinco parámetros opcionales. El objeto userPro es un prototipo (que no debe confundirse con la propiedad prototipo). El problema con esta implementación se hace evidente cuando se mira el uso de forma aislada:

```
var newUser = createUser('Tonya', '', '', '', 'dark');
```

Lo que salta inmediatamente es que es imposible saber cuál es el segundo, tercer o cuarto parámetro sin mirar la implementación createUser (). También es imposible establecer el último parámetro sin pasar valores para todos los parámetros. Es más, si desea agregar más parámetros más adelante o cambiar el orden de los parámetros, va a ser difícil si la función se utiliza con frecuencia.

Aquí está una mejor alternativa:

```
var newUser = createUser({
    name: 'Mike',
    showInSearch: false
});
```

Puede implementar esto fácilmente usando el método **extend** que viene con las bibliotecas más populares (incluyendo jQuery y Underscore). Así es como se hace con jQuery:

```
function createUser(options) {
    return $.extend({}, userProto, options);
}
```

$.extend() toma objetos como sus parámetros. El primero es el objeto a ser extendido. En este caso, queremos devolver un objeto nuevo para que no alteremos los objetos userProto o el objeto opciones. Los otros objetos (tantos como quieras) contienen las propiedades y los métodos con los que deseas extender el primer objeto. Esta es una forma sencilla y elegante de reutilizar código.


##Funciónes Polimorficas

En informática, el polimorfismo significa que algo se comporta de manera diferente en función del contexto, como las palabras que tienen diferentes significados en función de cómo se usan:

- ¡Cuidado con ese giro agudo en el camino!
- ¡Ese cuchillo es agudo!
- "John Resig es agudo! Hacer la función jQuery polimórfica fue un golpe de genio."


Las funciones polimórficas se comportan de manera diferente en función de los parámetros que se pasan a ellos. En JavaScript, esos parámetros se almacenan en el array-like arguments object, pero faltan los métodos útiles del Array.

**Array.prototype.slice()** es una forma fácil de copiar superficialmente algunos o todos de un Array (o un objeto similar a un Array).
Puede tomar prestado el método **.slice()** del prototipo Array mediante una técnica denominada delegación de métodos. Usted delega la llamada **.slice()** al objeto **Array.prototype.** La llamada al método tiene este aspecto:

```
var args = Array.prototype.slice.call(arguments, 0);
```

Slice comienza en el índice 0 y devuelve todo de ese índice como una nueva Array. Esa sintaxis es un poco larga, sin embargo. Es más fácil y rápido escribir:

```
var args = [].slice.call(arguments, 0);
```

La notación de corchete crea un nuevo Array vacío para delegar la llamada de slice(). Suena como que puede ser lento, pero la creación de un Array vacío es en realidad una operación muy rápida. Hice una prueba de rendimiento A/B con millones de operaciones y no vi un blip en el uso de memoria ni ninguna diferencia estadísticamente significativa en la velocidad de operación.

Puede utilizar esta técnica para crear una función que ordena los parámetros:

```
function sort() {
    var args = [].slice.call(arguments, 0);
    return args.sort();
}
test('Sort', function() {
    var result = sort('b', 'a', 'c');
    ok(result, ['a', 'b', 'c'], 'Sort works!');
});
```

Dado que los argumentos no son un Array real, no tiene el método .sort(). Sin embargo, como se devuelve un Array real de la llamada .slice(), tiene acceso a todos los métodos del Objeto Array en el Array args. El método .sort() devuelve una versión ordenada del Array.

Las funciones polimórficas frecuentemente necesitan examinar el primer argumento para decidir cómo responder. Ahora que args es un Array real, puede usar el método .shift() para obtener el primer argumento:

```
var first = args.shift();
```

Ahora se puede derivar si se pasa una cadena como primer parámetro:

```
function morph(options) {

    var args = [].slice.call(arguments, 0),
        animals = 'turtles'; // Set a default
    if (typeof options === 'string') {
        animals = options;
        args.shift();
    }
    return ('The pet store has ' + args + ' ' + animals + '.');
    
}

test('Polymorphic branching.', function() {
    
    var test1 = morph('cats', 3),
        test2 = morph('dogs', 4),
        test3 = morph(2);
        
    equal(test1, 'The pet store has 3 cats.', '3 Cats.');
    equal(test2, 'The pet store has 4 dogs.', '4 Dogs.');
    equal(test3, 'The pet store has 2 turtles.', 'The pet store has 2 turtles.');
});
```

###Method dispatch

El Dynamic dispatch (distribución dinámica) es el mecanismo que determina qué hacer cuando un objeto recibe un mensaje. JavaScript hace esto comprobando si el método existe en el objeto. Si no lo hace, el motor de JavaScript comprueba el objeto prototipo. Si el método no está allí, verifica el prototipo del prototipo, y así sucesivamente. Cuando encuentra un método de coincidencia, llama al método y pasa los parámetros. También se conoce como behavior delegation (delegación de comportamiento) en lenguajes prototipo basados en delegación como JavaScript.

El Dynamic dispatch (distribución dinámica) activa el polimorfismo seleccionando el método apropiado para ejecutar en función de los parámetros que se pasan al método en tiempo de ejecución. Algunos idiomas tienen sintaxis especial para admitir el Dynamic dispatch (distribución dinámica). En JavaScript, puede comprobar los parámetros desde el método llamado y llamar a otro método en respuesta:

```
var methods = {
        init: function(args) {
            return 'initializing...';
        },
        hello: function(args) {
            return 'Hello, ' + args;
        },
        goodbye: function(args) {
            return 'Goodbye, cruel ' + args;
        }
    },
    greet = function greet(options) {
        var args = [].slice.call(arguments, 0),
            initialized = false,
            action = 'init'; // init will run by default
        if (typeof options === 'string' &&
            typeof methods[options] === 'function') {
            action = options;
            args.shift();
        }
        return methods[action](args);
    };
    
test('Dynamic dispatch', function() {
    var test1 = greet(),
        test2 = greet('hello', 'world!'),
        test3 = greet('goodbye', 'world!');
    equal(test2, 'Hello, world!', 'Dispatched to hello method.');
    equal(test3, 'Goodbye, cruel world!', 'Dispatched to goodbye method.');
});
```

Este estilo manual de envío dinámico es una técnica común en los complementos de jQuery para permitir a los desarrolladores añadir muchos métodos a un plug-in sin agregarlos al prototipo jQuery (jQuery.fn). Utilizando esta técnica, puede reclamar un solo nombre en el prototipo jQuery y agregar tantos métodos como desee. A continuación, los usuarios seleccionan el método con el que quieren invocar:

```
$(selection).yourPlugin('methodName', params);
```
##Generics and Collection Polymorphism (Genéricos y Colección Polimorfica)

La programación genérica es un estilo que intenta expresar algoritmos y estructuras de datos de una manera que sea agnóstica de tipo. La idea es que la mayoría de los algoritmos pueden ser empleados a través de una variedad de diferentes tipos. La programación genérica normalmente comienza con una o más implementaciones específicas de tipo, que luego se levantan (abstraídas) para crear una versión más genérica que funcionará con un nuevo conjunto de tipos.

Los genéricos no requieren lógica condicional de ramificación para implementar un algoritmo de forma diferente en función del tipo de datos que se pasan. En su lugar, los tipos de datos pasados deben apoyar las características necesarias que el algoritmo necesita para trabajar. Esas características son llamados requisitos, que a su vez quedar recogidos en conjuntos llamados conceptos.

Los genéricos emplean el polimorfismo paramétrico, que utiliza una sola rama de la lógica aplicada a los parámetros genéricos del tipo. Por el contrario, el polimorfismo ad hoc se basa en la ramificación condicional para manejar el tratamiento de diferentes tipos de parámetros (ya sea incorporado en el lenguaje con características como el envío dinámico o introducido en el tiempo de diseño del programa).

La programación genérica es particularmente relevante para la programación funcional porque la programación funcional funciona mejor cuando un vocabulario simple de funciones puede expresar una amplia gama de funcionalidades, independientemente del tipo.

En la mayoría de los idiomas, la programación genérica se ocupa de hacer que los algoritmos funcionen para diferentes tipos de listas. En JavaScript, cualquier colección (Array u objeto) puede contener cualquier tipo (o mezcla de tipos), y la mayoría de los programadores confían en la tipificación de patos para lograr objetivos similares. (Si camina como un pato y cuacla como un pato, trátelo como un pato.) En otras palabras, si un objeto tiene las características que necesita, asuma que es el tipo correcto de objeto y lo usa.) Muchas de las funciones incorporadas Los métodos de objeto son genéricos, lo que significa que pueden funcionar en múltiples tipos de objetos.

JavaScript soporta dos tipos de colecciones: objetos y arrays. La diferencia principal entre un objeto y un Array es que uno está marcado con nombres y el otro secuencialmente con números. Los objetos no garantizan ningún orden particular; Arrays hacer. Aparte de eso, ambos se comportan prácticamente igual. A menudo tiene sentido implementar funciones que funcionan independientemente del tipo de colección que se pasa.

Muchas de las funciones que puede aplicar a un Array también funcionan para un objeto y viceversa. Por ejemplo, digamos que desea seleccionar un miembro aleatorio de un objeto o Array.
La forma más fácil de seleccionar un elemento aleatorio es utilizar un índice numerado, por lo que si la colección es un objeto, podría convertirse a un Array utilizando el polimorfismo ad-hoc. La siguiente función hará eso:

```
var toArray = function toArray(obj) {
    var arr = [],
        prop;
    for (prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            arr.push(prop);
        }
    }
    return arr;
};
```

La función randomItem() es fácil ahora. Primero, pruebe el tipo de colección que se pasa y lo convierte a una Array si no es uno ya, y luego devolver un elemento aleatorio del Array mediante el método incorporado Math.random ():

```
var randomItem = function randomItem(collection) {
    var arr = ({}.toString.call(collection) !==
            '[object Array]') ?
        toArray(collection) :
        collection;
    return arr[Math.floor(arr.length * Math.random())];
};

test('randomItem()', function() {
    varobj = {
            a: 'a',
            b: 'b',
            c: 'c'
        },
        arr = ['a', 'b', 'c'];
    ok(obj.hasOwnProperty(randomItem(obj)), 'randomItem works on Objects.');
    ok(obj.hasOwnProperty(randomItem(arr)), 'randomItem works on Arrays.');
});
```

##Method Chaining and Fluent APIs (Encadenamiento de métodos y APIs fluidas)

El encadenamiento de método utiliza la salida de una llamada de método como el contexto de la siguiente llamada de método. Por ejemplo, en jQuery a menudo se ven cosas como:

```
$('.friend').hide().filter('.active').show();
```

Perhaps better:

```
$('.friend')
    .hide()
    .filter('.active')
    .show();
```

Esto se traduce en: "Buscar todos los elementos con la clase de amigo y ocultarlos, luego encontrar a los amigos con la clase activa y mostrarlos".

En la carga de la página, nuestra lista de amigos se ve así:

- Mick- Hunz (active)- Yannis

Después de ejecutar el código anterior, se ve así:

- Hunz (active)

Uno de los principales beneficios del encadenamiento de métodos es que puede utilizarse para soportar API fluidas. En resumen, una API fluida es una que se lee como lenguaje natural. Eso no significa que tiene que parecerse al inglés, pero las API fluidas suelen usar verbos reales como llamadas de método (como ocultar y mostrar).

JQuery es un gran ejemplo de una API fluida. De hecho, la fluidez de jQuery hace que sea una de las bibliotecas más fáciles de aprender y usar. Casi todo lo que hace jQuery ya estaba disponible en otras bibliotecas cuando jQuery fue lanzado por primera vez. Lo que hizo jQuery destacar fue el vocabulario fácil de aprender. Casi todas las sentencias jQuery se leen de la siguiente manera: "Encuentre todos los elementos que coincidan con un selector, luego haga x, luego y, luego z. Selección, verbo, verbo ... "

El encadenamiento tiene sus desventajas. Se puede animar a hacer demasiado en una sola línea de código, puede animar a escribir demasiado código de procedimiento, y puede ser difícil de depurar. Es difícil establecer un punto de interrupción en medio de una cadena.

Si usted entra en un punto apretado de depuración de una cadena, recuerde que siempre puede capturar la salida en cualquier paso de la cadena con una asignación de variables y reanudar la cadena llamando al siguiente método en esa variable. De hecho, usted no tiene que encadenar en absoluto para usar una API fluida.

Hay más en la fluidez que en el encadenamiento. El punto destacado es que los métodos fluidos se hacen para trabajar juntos para expresar la funcionalidad de la misma manera que las palabras en un lenguaje trabajan juntas para expresar ideas. Esto significa que los objetos de salida con métodos que tienen sentido para llamar a los datos resultantes.
La creación de una API fluida es muy similar a la construcción de un lenguaje específico de dominio en miniatura (DSL). Vamos a entrar en detalles sobre cómo construir una API fluida en el capítulo 3.

Es fácil ir demasiado lejos con API fluidas. En otros idiomas, las API fluidas se utilizan frecuentemente para configurar un nuevo objeto. Dado que JavaScript admite la notación literal de objetos, esto es casi con toda seguridad un mal uso de la fluidez en JavaScript.
Fluidez también puede conducir a verbosidad innecesaria. Por ejemplo, la API de Should.js le anima a escribir oraciones largas con acceso a la notación de puntos. Mantenlo simple.

##Functional Programming (Programación Funcional)