Unix
===

Gran parte de Unix gira en torno a ficheros, Cada archivo posee un nombre, datos y metadatos.

Comandos
==

[Link de documentación](https://www.hscripts.com/es/tutoriales/linux-commands/)

**ls**: Lista el contenido de ficheros en un directorio.

**cat**: El comando ‘cat’ imprimirá por pantalla el contenido del fichero sin ningún tipo de paginación ni posibilidad de modificarlo. Básicamente concatena archivos o la salida estándar en la salida estándar.

Mostrar el contenido de un fichero:

``
cat fichero
``

Concatenar dos ficheros de texto en uno:

``
cat fichero1 fichero2 > fichero3
``


**more**: Al igual que ‘cat’, ‘more’ permite visualizar por pantalla el contenido de un fichero de texto, con la diferencia con el anterior de que ‘more’ pagina los resultados. Primero mostrará por pantalla todo lo que se pueda visualizar sin hacer scroll y después, pulsando la tecla espacio avanzará de igual modo por el fichero.


**less**: El comando ‘less’ es el más completo de los tres, pues puede hacer todo lo que hace ‘more’ añadiendo mayor capacidad de navegación por el fichero (avanzar y retroceder), recomendable.

``
less --help
``

**cp**: Se utiliza para copiar ficheros.

Un ejemplo real de uso, podría ser el realizar un backup de un fichero:

``
cp log.txt log.bak
``


Un ejemplo en el cual pasamos un archivo llamado “log.txt” al directorio /home/usuario/logs/ utilizando el comando cp en Linux, podría ser el siguiente:

``
cp log.txt /home/usuario/logs/
``

**mv**: Mueve un archivo especifico.

``
mv ejemplo.sh /home/korn/desktop/.
``

**rm**: Remueve un archivo especifico.

``
rm ejemplo.sh
``

**file**: Muestra la informacion de un fichero.

``
file Comandos.md 
Comandos.md: UTF-8 Unicode text, with very long lines
``

**pwd**: Muestra la ruta de donde me encientro.

**mkdir**: Crear un directorio

**rmdir**: Remueve un directorio

**stat**: Muestra toda la informacion disponible de un nodo o fichero



Directorios UNIX
===

![Sin titulo](img/directorios-interes.png)
