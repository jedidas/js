/*

Auto-clausuras en JS: el fundamento de las propiedades privadas


Declaración vs Definición de variables en JavaScript

Una variable en JavaScript se declara de dos formas diferentes: mediante la palabra clave “var”, o bien directamente nombrándola por primera vez cuando se le asigna un valor (sin poner var delante, lo cual es muy mala práctica, por cierto).

El hecho de declarar una variable no implica tener que definirla, ya que la podemos declarar pero no necesariamente asignarle un valor:
*/

var variable1;  //Se declara pero no se define 
variable1 = “1”; //Se define asignándole un valor 

var variable2 = 2; //Se declara y se define todo a la vez

/*
Ahora que tenemos claro este concepto, podemos entender mejor lo que ocurre en el ejemplo anterior para que se muestre un undefined por pantalla.

El efecto de elevación

Aunque JavaScript es un lenguaje interpretado y procesa las líneas de código una a una según se las va encontrando, en realidad no es del todo así. En el caso de las variables que hay dentro de una función lo que hace el intérprete es declararlas todas a la vez al principio de la función, independientemente de donde estén realmente declaradas dentro de ésta. Por eso, en el ejemplo anterior aunque la variable está declarada y definida abajo del todo, cuando la vamos a mostrar nos devuelve un “undefined”: el intérprete “eleva” la declaración implícitamente al principio de la función, así que el código anterior es equivalente a este:
*/

//Variable global 
var name = "Jose";

function HelloWorld(){ 
   //Variable local 
   var name; 
   alert(name); 
   name = "Pepe"; 
}

HelloWorld();

/*
Si vemos el código de esta manera entonces el resultado cobra todo el sentido del mundo ¿verdad?
Si quitamos el doble paréntesis de antes (de la línea 11) que fuerza la ejecución, podemos trabajar con ella como si de una clase normal se tratara y seguimos teniendo acceso limitado a los miembros privados:
*/

var Coche = function() { 
  var modelo= "Ninguno";
  return {
    getModelo : function () {
      return modelo;
    },
    setModelo : function (sModelo) {
      modelo = sModelo.toUpperCase();
    }
  };
};

var coche1= new Coche();
var coche2= new Coche();
alert(coche1.modelo);  //Undefined
coche1.setModelo("Porsche");
coche2.setModelo("Smart");
alert(coche1.getModelo()); //PORSCHE
alert(coche2.getModelo()); //SMART



/*
Una implementación del patrón singleton en Javascript es la siguiente:

*/
var Singleton = function SingletonConstructor() {
    return { 
        getInstance : function Singleton$getInstance() {
            return this;
        }
    };
}();