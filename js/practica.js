var nameSpace = (function() {

    //Variable privada donde almaceno todos los objetos creados
    var todos = [];

    ///Tipos de usuarios u objetos que se pueden crear
    var user = {
        name: '',
        type: 'user'
    };

    var admin = {
        name: '',
        type: 'user'
    };

    var gerente = {
        name: '',
        type: 'user'
    };

    //Fabrica aqui se crean los usuarios
    var FactoryUser = function(name, type, values) {



        /*Metodo que crea los usuarios
		Se le pasa 3 valores el nombre del objeto, el tipo y los valores o propiedades y metodos que contendra 
    	*/
        var createUser = function() {
            var usuario = {};
            //Valoramos el tipo y contruimos el objeto a devolver
            switch (type) {
                case 'user':
                    usuario = user;
                    usuario.name = name;
                    usuario.datos = values;
                    break;
                case 'admin':
                    usuario = admin;
                    usuario.name = name;
                    usuario.datos = values;
                    break;
                case 'gerente':
                    usuario = gerente;
                    usuario.name = name;
                    usuario.datos = values;
                    break;
            }
            return usuario;
        }
        return createUser();
    };

    //Singelton
    /*
	Metodo singelton para almacenar el valor de los datos a enviar
    */
    var Singelton = function() {

        var dataValue = [],
            getUser = null,
            serUser = null;

        getter = function() {
            return dataValue;
        };
        setter = function(argument) {
            dataValue = argument;
        };

        return {
            getter: getter,
            setter: setter
        }

    };

    /*
	Guardo datos  o los actuliza
    */
    var saveDate = function(name, datos) {
        var get = null,
            set = null,
            valores = null;

        //Si se envia el nombre crea un nuevo objeto con ese nombre
        if (arguments[0]) {
            todos[name] = {
                //Al objeto le creo un metodo singelton
                valores: new Singelton()
            };
            //Le envio el valor al metodo
            todos[name].valores.setter(datos);
            //console.log(todos[name].valores.getter());
        }
        //Devuelve los valores del objeto por el que se pregunta
        get = function(nameData) {
            if (!nameData) {
                return 'Nombre incorrecto';
            }
            return todos[nameData].valores.getter();
        };
        //Actualiza un objeto existente
        set = function(nameData, newArgument) {
            return todos[nameData].valores.setter(newArgument);
        };

        return {
            get: get,
            set: set
        };

    }



    return {
        createUser: FactoryUser,
        save: saveDate
    };

})();



var myApp = nameSpace,
    OtherApp = nameSpace;
//Creo los nuevos objetos o usuarios
myApp.save('Campo1', {
    campo: 'Hola',
    valor: 'Este es Campo1'
});
myApp.save('Campo2', {
    campo: 'Ya volvi Campo2'
});
OtherApp.save('valor1', {
    campo: 'Adios',
    valor: 'Aqui va valor1'
});
OtherApp.save('valor2', {
    campo: 'Adios',
    valor: 'Aqui va valor2'
});

//Consulto los valores de los objetos creados
console.log(myApp.save().get('Campo1'));
console.log(myApp.save().get('Campo2'));

console.log(OtherApp.save().get('valor1'));
console.log(OtherApp.save().get('valor2'));

//Actualizamos los valores de los objetos creados
myApp.save().set('Campo1', {
    text: 'Sobre escribo Campo1'
});
OtherApp.save().set('valor1', {
    text: 'Sobre escribo valor1'
});

console.log(myApp.save().get('Campo1'));



var list = [];
//Ceamos usuarios y los almacenamos en un array
list.push(myApp.createUser('Rosario', 'user', {
    pass: 'chy',
    web: 'yahoo.com',
    empleo: 'Publicista'
}));

list.push(myApp.createUser('David', 'gerente', {
    pass: 'Jedidias',
    web: 'dgweb.com',
    empleo: 'FrontEnd'
}));

list.push(myApp.createUser('Maria', 'admin', {
    pass: 'chy',
    web: 'yahoo.com',
    empleo: 'Publicista'
}));


console.log(list);





/*
====================================================
					AbstractFactory
====================================================


Permite crear objetos sin que el Factory sepa que clase objeto son

Consta de 3 partes basicas
-Un objeto privado que guarda las clases registradas.
-Un metodo publivo para registrar nuevas clases
-Un metodo publico para construir nuevos objetos partiendo de las clases ya registradas en el Objeto privada

El AbstractFactory es una funcion autoejecutable para que sus metodos y propiedades permanescan privados
*/

var AbstractFactory = (function(argument) {

    //Objeto privado que almacena clases
    var registroClases = [];

    function registrarClase(nombre, valoresClase) {
        //En el array almaceno la nueva clase cuto indice sea el nombre de la clase
        registroClases[nombre] = valoresClase;
        //Retorno algo en este caso el array de clases
        return registroClases[nombre];
    }

    function contruirObjeto(nombre, valoresObjeto) {
        //Busco la clase registrada por el indice del nombre
        var nuevoObjeto = registroClases[nombre];
        //Si la clase existe, Creo una nueva instancia y le paso los valores al objeto, si no retorno null
        return nuevoObjeto ? new nuevoObjeto(valoresObjeto) : null;
    }

    //Retorno los metodos
    return {
        registrarClase: registrarClase,
        contruirObjeto: contruirObjeto
    }


})();

//Registramos dos clases
AbstractFactory.registrarClase('perro',
    function(params) {
        // setup o valores por default
        this.color = params.color || "Negro";
        this.raza = params.raza || "Labrador";
        this.nombre = params.nombre || "Buddy";
    });

AbstractFactory.registrarClase('gato',
    function(params) {
        this.color = params.color || "Blanco";
        this.cazador = params.cazador || true;
        this.nombre = params.nombre || "Misu";
    });


// instanciamos un nuevo objeto
var bichoFeo = AbstractFactory.contruirObjeto("gato", {
    nombre: "Garfiel",
    color: 'Negro asabache'
});

bichoFeo.trabajo = function() {
    return 'No hacer nada';
};

console.log(bichoFeo);
console.log(bichoFeo.trabajo());