'use strict';

$.fn.is_on_screen = function() {
    var win = $(window);
    var viewport = {
        top: win.scrollTop(),
        left: win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
};

//Agregar clase a elementos visibles
$scrollVisible = $('.scrollVisible');
var nodosVisible = function() {
    if ($scrollVisible.length) { // if target element exists in DOM
        //Recorro cada elemento
        $scrollVisible.each(function(index, val) {
            if ($(val).is_on_screen()) {
                $(val).hasClass('active-screen') ? '' : $(val).addClass('active-screen');

                if ($(val).attr('data-anim')) {
                    $(val).addClass($(val).attr('data-anim'));
                }

            } else {
                $(val).hasClass('active-screen') ? $(val).removeClass('active-screen') : '';

                if ($(val).attr('data-anim')) {
                    $(val).removeClass($(val).attr('data-anim'));
                }
            }
        });
    }
}

//Al hacer scroll verifico la visibilidad de los nodos
$(window).scroll(function() { // bind window scroll event
    nodosVisible();
});
nodosVisible();