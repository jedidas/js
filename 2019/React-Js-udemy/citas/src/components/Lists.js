import React, { Component } from 'react'

import ListItem from './ListItem'

class Lists extends Component {
    state = {}

    checkDatesMessage() {

        if (!this.props.dates.length) {
            return 'No hay citas'
        }
        return 'Administra tus citas aqui'

    }

    render() {
        return (
            <div className="card mt-5">
                <div className="card-body">
                    <h2 className="card-title text-center">{this.checkDatesMessage()}</h2>
                    <div className="lista-cita list-group">
                        
                        {Object.keys(this.props.dates).map((key) =>  {
                            return <ListItem
                                key={this.props.dates[key].id}
                                info={this.props.dates[key]}
                                deleteDate={this.props.deleteDate}
                            />
                        })}
                        
                    </div>
                </div>
            </div>
        );
    }
}

export default Lists;