import React, { Component } from 'react'
import uuid from 'uuid'

class DatesForm extends Component {
    state = {
        name: null,
        owner: null,
        date: null,
        hour: null,
        symptom: null
    }

    name = React.createRef();
    owner = React.createRef();
    date = React.createRef();
    hour = React.createRef();
    symptom = React.createRef();

    addDate = (e) => {

        e.preventDefault();

        let date = {
            id: uuid(),
            name: this.name.current.value,
            owner: this.owner.current.value,
            date: this.date.current.value,
            hour: this.hour.current.value,
            symptom: this.symptom.current.value,
        };

        if (this.validateForm(date)) {
            this.props.addDate(date);
            e.currentTarget.reset();
        }

    }

    validateForm(date) {

        let newState = {
            ...this.state
        };

        Object.keys(date).map((key) => {

            if ((typeof date[key] === 'string' && !date[key].trim().length) || (typeof date[key] === 'number' && !date[key].length)) {
                newState[key] = false;
            } else {
                newState[key] = true;
            }
            return null

        })

        this.setState(newState);

        for (const key in newState) {
            if (newState[key] === false) {
                return false;
            }
        }

        return true;

    }

    showErrorMessage(error, input) {

        if (this.state[input] === null) {
            return null;
        }

        if (this.state[input] === true) {
            return null;
        }

        return (
            <div className="invalid-feedback">{error}</div>
        )
    }

    showErrorClass(input) {

        if (this.state[input] !== false) {
            return 'form-control';
        }

        return 'form-control is-invalid';

    }

    render() {
        return (
            <div className="card-body">
                <form onSubmit={this.addDate} >

                    <div className="row">
                        <label className="col-sm-4 col-lg-2 col-form-label">Nombre Mascota</label>
                        <div className="form-group  has-danger col-sm-8 col-lg-10">
                            <input type="text" className={this.showErrorClass('name')} placeholder="Nombre Mascota" ref={this.name} />
                            {this.showErrorMessage('Campo requerido', 'name')}
                        </div>
                    </div>

                    <div className="row">
                        <label className="col-sm-4 col-lg-2 col-form-label">Nombre Dueño</label>
                        <div className="form-group  has-success col-sm-8 col-lg-10">
                            <input type="text" className={this.showErrorClass('owner')} placeholder="Nombre Dueño de la Mascota" ref={this.owner} />
                            {this.showErrorMessage('Campo requerido', 'owner')}
                        </div>
                    </div>

                    <div className="row">
                        <label className="col-sm-4 col-lg-2 col-form-label">Fecha</label>
                        <div className="form-group col-sm-8 col-lg-4  mb-4 mb-lg-0">
                            <input type="date" className={this.showErrorClass('date')} ref={this.date} />
                            {this.showErrorMessage('Campo requerido', 'date')}
                        </div>

                        <label className="col-sm-4 col-lg-2 col-form-label">Hora</label>
                        <div className="form-group col-sm-8 col-lg-4">
                            <input type="time" className={this.showErrorClass('hour')} ref={this.hour} />
                            {this.showErrorMessage('Campo requerido', 'hour')}
                        </div>
                    </div>

                    <div className="row">
                        <label className="col-sm-4 col-lg-2 col-form-label">Sintomas</label>
                        <div className="form-group col-sm-8 col-lg-10">
                            <textarea className={(this.state.symptom !== false) ? 'form-control' : 'form-control is-invalid'} ref={this.symptom} ></textarea>
                            {this.showErrorMessage('Campo requerido', 'symptom')}
                        </div>
                    </div>
                    <div className="row justify-content-end">
                        <div className="col-sm-3">
                            <button type="submit" className="btn btn-success w-100">Agregar</button>
                        </div>
                    </div>

                </form>
            </div>
        );
    }
}

export default DatesForm;