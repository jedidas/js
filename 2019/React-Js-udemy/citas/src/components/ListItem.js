import React, { Component } from 'react'

class ListItem extends Component {
    state = {  }

    deleteDate() {
        this.props.deleteDate(this.props.info.id);
    }

    render() { 
        return (
            <div className="media list-group-item">
                <div className="media-body">
                    <h3 className="mt-0">Mascota</h3>
                    <p className="card-text"><strong>Nombre:</strong> {this.props.info.name}</p>
                    <p className="card-text"><strong>Dueño:</strong> {this.props.info.owner}</p>
                    <p className="card-text"><strong>Fecha:</strong> {this.props.info.date}</p>
                    <p className="card-text"><strong>Hora:</strong> {this.props.info.hour}</p>
                    <p className="card-text"><strong>Sintomas:</strong> {this.props.info.symptom}</p>
                    <button type="button" className="btn btn-danger" onClick={this.deleteDate.bind(this)}>Eiminar Cita</button>
                </div>
            </div>
        );
    }
}
 
export default ListItem;