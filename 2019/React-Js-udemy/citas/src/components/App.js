import React, { Component } from 'react';

import '../css/App.css'

import DatesForm from './DatesForm'
import Lists from './Lists'

class App extends Component {

  state = {
    dates: []
  }

  componentDidMount() {
    console.log('componentDidMount');
    let datesStorage = localStorage.getItem('dates');
    if (datesStorage) {
      this.setState({
        dates: JSON.parse(datesStorage)
      });
    }

  }

  componentWillMount() {
    console.log('componentWillMount');
  }

  componentWillUnmount() {
    console.log('componentWillUnmount');

  }

  componentDidUpdate() {
    console.log('componentDidUpdate');
    localStorage.setItem('dates', JSON.stringify(this.state.dates));
  }

  addDate = (data) => {

    let newDates = [...this.state.dates, data];

    this.setState({
      ...this.state,
      dates: newDates
    });

  }

  deleteDate = (id) => {

    let dates = [...this.state.dates];
    dates = dates.filter(date => date.id !== id);
    this.setState({
      dates
    });

  }

  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-md-6">

              <div className="card mt-5">
                <DatesForm
                  addDate={this.addDate}
                />
              </div>

            </div>
            <div className="col-xs-12 col-md-6">
              <Lists
                dates={this.state.dates}
                deleteDate={this.deleteDate}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
