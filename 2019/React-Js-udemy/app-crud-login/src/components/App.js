import React, {
  Component
} from 'react';

import './../css/App.css';

import FirebaseApp from './../util/FirebaseApp'
import AppRouter from './AppRouter'

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      auth: {
        state: true,
        user: null
      }
    }

    this.register = new FirebaseApp();
    this.observerSession();

  }



  registerUser = (data) => {

    this.register.set(data).then((sucess) => {

      console.log('register sucess');
      console.log(sucess.additionalUserInfo.isNewUser);
      if (!sucess.user.emailVerified) {
        console.log('Senbd Email', sucess.user.emailVerified);

        this.register.completeProfile(data);
        this.register.sendEmailVerification();
      }

    }).catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
      console.log(errorCode, errorMessage);

    });

  }

  signIn = (data) => {

    this.register.signIn(data).then((sucess) => {

      console.log('login sucess');
      console.log(sucess.additionalUserInfo.isNewUser);

    }).catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
      console.log(errorCode, errorMessage);

    });

  }

  observerSession() {
    this.register.observer((user) => {
      if (user) {

        this.setState({
          ...this.state,
          auth: {
            state: true,
            user: user
          }
        });

        this.register.setUser(user);

      } else {
        console.log('No user');
        this.setState({
          ...this.state,
          auth: {
            state: false,
            user: null
          }
        });
      }
    });
  }

  signOut = (e) => {
    e.preventDefault();
    this.register.signOut().then((sucess) => {
      console.log('signOut sucess');
    }).catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
      console.log(errorCode, errorMessage);

    });

  }

  componentDidUpdate() {

  }

  render() {
    return <AppRouter
    data = {
      this.state
    }
    auth = {
      this.state.auth
    }
    registerUser = {
      this.registerUser
    }
    signIn = {
      this.signIn
    }
    signOut = {
      this.signOut
    }
    />
  }
}

export default App;