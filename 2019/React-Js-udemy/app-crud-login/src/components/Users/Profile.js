import React, { Component } from 'react'

import Layout from './../layouts/Layout'

class Profile extends Component {

    render() {
        
        if (this.props.user && this.props.user.hasOwnProperty('emailVerified')) {
            if (!this.props.user.emailVerified) {
                return (
                    <Layout>
                        <h1>Verifica tu email</h1>
                    </Layout>
                )
            }
        }else {
            return null;
        }

        return (
            <Layout>
                <div className="col s12">
                    <h5>Bienvenido: {this.props.user.displayName} / {this.props.user.email}</h5>
                </div>
            </Layout>
        );
    }
}

export default Profile;