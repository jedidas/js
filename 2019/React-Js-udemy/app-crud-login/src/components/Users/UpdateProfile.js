import React, { Component } from 'react'

class UpdateProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <form className="col s12" onSubmit={(this.props.action === 'login') ? this.signIn : this.registerUser}>
                <h1 className="text-center">{this.props.action} User</h1>

                <div className="input-field col s12">
                    <i className="material-icons prefix">border_outer</i>
                    <input placeholder="Nombre" id="name" type="text" className="validate" ref={this.name} />
                    <label htmlFor="name" className="active">Nombre</label>
                    {this.showErrorMessage('Campo requerido', 'name')}
                </div>

                <div className="input-field col s12">
                    <i className="material-icons prefix">account_circle</i>
                    <input id="email" type="email" className="validate" ref={this.email} />
                    <label className="active" htmlFor="Email">Email</label>
                    {this.showErrorMessage('Campo requerido', 'email')}
                </div>


                <div className="input-field col s12">
                    <i className="material-icons prefix">border_outer</i>
                    <input id="password" type="password" className="validate" ref={this.password} />
                    <label className="active" htmlFor="password">Password</label>
                    {this.showErrorMessage('Campo requerido', 'password')}
                </div>

                <div className="file-field input-field">
                    <div className="btn">
                        <span>Avatar</span>
                        <input type="file" ref={this.photoURL} />
                    </div>
                    <div className="file-path-wrapper">
                        <input className="file-path validate" type="text" />
                    </div>
                    {this.showErrorMessage('Campo requerido', 'photoURL')}
                </div>

                <button className="btn waves-effect waves-light" type="submit" name="action">Submit <i className="material-icons right">send</i></button>

            </form >
        );
    }
}

export default UpdateProfile;