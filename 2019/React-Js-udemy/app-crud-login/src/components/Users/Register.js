import React, { Component } from 'react'

import Layout from './../layouts/Layout'

class Register extends Component {

    name = React.createRef();
    email = React.createRef();
    password = React.createRef();
    photoURL = React.createRef();

    constructor(props) {
        super(props);
        this.state = {
            name: null,
            email: null,
            password: null,
            photoURL: null,
        }

    }


    validateForm(date) {

        let newState = {
            ...this.state
        };

        Object.keys(date).map((key) => {

            if ((typeof date[key] === 'string' && !date[key].trim().length) || (typeof date[key] === 'number' && !date[key].length)) {
                newState[key] = false;
            } else {
                newState[key] = true;
            }
            return null

        })

        this.setState(newState);

        for (const key in newState) {
            if (newState[key] === false) {
                return false;
            }
        }

        return true;

    }

    showErrorMessage(error, input) {

        if (this.state[input] === null) {
            return null;
        }

        if (this.state[input] === true) {
            return null;
        }

        return (
            <div className="invalid-feedback">{error}</div>
        )
    }

    showErrorClass(input) {

        if (this.state[input] !== false) {
            return 'form-control';
        }

        return 'form-control is-invalid';

    }

    registerUser = (e) => {
        e.preventDefault();

        let date = {
            name: this.name.current.value,
            email: this.email.current.value,
            password: this.password.current.value,
            photoURL: this.photoURL.current.value,
        };

        if (this.validateForm(date)) {
            this.props.registerUser(date);
        }

    }

    takeAction = (e) => {
        e.preventDefault();
        switch (this.props.action) {
            case 'login': {
                return this.signIn();
            }
            case 'register': {
                return this.registerUser();
            }
            default: {
                return this.signIn;
            }
        }

    }

    render() {

        return (
            <Layout>
                <form className="col s12" onSubmit={this.registerUser}>
                    <h1 className="text-center">{this.props.action} User</h1>

                    <div className="input-field col s12">
                        <i className="material-icons prefix">border_outer</i>
                        <input placeholder="Nombre" id="name" type="text" className="validate" ref={this.name} />
                        <label htmlFor="name" className="active">Nombre</label>
                        {this.showErrorMessage('Campo requerido', 'name')}
                    </div>

                    <div className="input-field col s12">
                        <i className="material-icons prefix">account_circle</i>
                        <input id="email" type="email" className="validate" ref={this.email} />
                        <label className="active" htmlFor="Email">Email</label>
                        {this.showErrorMessage('Campo requerido', 'email')}
                    </div>


                    <div className="input-field col s12">
                        <i className="material-icons prefix">border_outer</i>
                        <input id="password" type="password" className="validate" ref={this.password} />
                        <label className="active" htmlFor="password">Password</label>
                        {this.showErrorMessage('Campo requerido', 'password')}
                    </div>

                    <div className="file-field input-field col s12">
                        <div className="btn">
                            <span>Avatar</span>
                            <input type="file" ref={this.photoURL} />
                        </div>
                        <div className="file-path-wrapper">
                            <input className="file-path validate" type="text" />
                        </div>
                        {this.showErrorMessage('Campo requerido', 'photoURL')}
                    </div>

                    <button className="btn waves-effect waves-light" type="submit" name="action">Submit <i className="material-icons right">send</i></button>

                </form >
            </Layout>
        );
    }
}

export default Register;