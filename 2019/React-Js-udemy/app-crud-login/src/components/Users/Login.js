import React, { Component } from 'react'

import Layout from './../layouts/Layout'

class Login extends Component {

    email = React.createRef();
    password = React.createRef();

    constructor(props) {
        super(props);
        this.state = {
            email: null,
            password: null
        }

    }


    validateForm(date) {

        let newState = {
            ...this.state
        };

        Object.keys(date).map((key) => {

            if ((typeof date[key] === 'string' && !date[key].trim().length) || (typeof date[key] === 'number' && !date[key].length)) {
                newState[key] = false;
            } else {
                newState[key] = true;
            }
            return null

        })

        this.setState(newState);

        for (const key in newState) {
            if (newState[key] === false) {
                return false;
            }
        }

        return true;

    }

    showErrorMessage(error, input) {

        if (this.state[input] === null) {
            return null;
        }

        if (this.state[input] === true) {
            return null;
        }

        return (
            <div className="invalid-feedback">{error}</div>
        )
    }

    showErrorClass(input) {

        if (this.state[input] !== false) {
            return 'form-control';
        }

        return 'form-control is-invalid';

    }

    signIn = (e) => {
        e.preventDefault();
        let date = {
            email: this.email.current.value,
            password: this.password.current.value,
        };

        if (this.validateForm(date)) {
            this.props.signIn(date);
        }
    }

    render() {

        return (
            <Layout>
                <form className="col s12" onSubmit={this.signIn}>
                    <h1 className="text-center">{this.props.action} User</h1>
                    <div className="input-field col s12">
                        <i className="material-icons prefix">account_circle</i>
                        <input id="email" type="email" className="validate" ref={this.email} />
                        <label className="active" htmlFor="Email">Email</label>
                        {this.showErrorMessage('Campo requerido', 'email')}
                    </div>

                    <div className="input-field col s12">
                        <i className="material-icons prefix">border_outer</i>
                        <input id="password" type="password" className="validate" ref={this.password} />
                        <label className="active" htmlFor="password">Password</label>
                        {this.showErrorMessage('Campo requerido', 'password')}
                    </div>

                    <button className="btn waves-effect waves-light" type="submit" name="action">Submit <i className="material-icons right">send</i></button>

                </form >
            </Layout>
        );
    }
}

export default Login;