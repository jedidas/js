import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";

import Header from './Header'
import NoMatch from './NoMatch'
import Home from './Home'
import Profile from './Users/Profile'
import Register from './Users/Register'
import Login from './Users/Login'

class AppRouter extends Component {



    render() {
        // const customHistory = createBrowserHistory();

        return (
            <Router>
                <React.Fragment>
                    <Header
                        auth={this.props.auth}
                        signOut={this.props.signOut}
                    />
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route path="/profile" render={() => {

                            if (!this.props.auth.state) {
                                return <Redirect to='/' />
                            }

                            return <Profile
                                user={this.props.auth.user}
                            />;

                        }} />
                        <Route path="/register" render={() => {

                            if (this.props.auth.state) {
                                return <Redirect to='/profile' />
                            }

                            return <Register
                                registerUser={this.props.registerUser}
                                action='register'
                            />;

                        }} />
                        <Route path="/login" render={() => {

                            if (this.props.auth.state) {
                                return <Redirect to='/profile' />
                            }

                            return (<Login
                                signIn={this.props.signIn}
                                action='login'
                            />);
                        }} />
                        <Route component={NoMatch} />
                    </Switch>
                </React.Fragment>
            </Router>
        );
    }
}

export default AppRouter;