import React, { Component } from 'react';
import { NavLink } from "react-router-dom";

class Header extends Component {

    showAuthLinks = () => {
        if (this.props.auth.state) {
            return (
                <React.Fragment>
                    <li><NavLink to={'/profile'}>Profile</NavLink></li>
                    <li><a href="#/" onClick={this.props.signOut}>Sign Out</a></li>
                </React.Fragment>
            )
        } else {
            return (
                <React.Fragment>
                    <li><NavLink to={'/register'}>Register</NavLink></li>
                    <li><NavLink to={'/login'}>Sign In</NavLink></li>
                </React.Fragment>
            )
        }
    }

    render() {

        return (
            <nav className="blue clearfix">
                <div className="container">
                    <div className="row">
                        <div className="nav-wrapper">
                            <a href="/" className="brand-logo">Logo</a>
                            <ul id="nav-mobile" className="right hide-on-med-and-down">
                                <li><NavLink to={'/'}>Home</NavLink></li>
                                {this.showAuthLinks()}
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Header;