import Firebase from './Firebase'

class FirebaseApp {

    constructor() {
        this.firebase = Firebase;
        this.user = null;
        this.database = this.firebase.firestore();
        this.database.settings({
            timestampsInSnapshots: true
        });
    }

    setUser(user) {
        this.user = user;
    }

    set(data) {
        return this.firebase.auth().createUserWithEmailAndPassword(data.email, data.password);
    }

    get(id) {

    }

    signIn(data) {
        return this.firebase.auth().signInWithEmailAndPassword(data.email, data.password);
    }

    signOut() {
        return this.firebase.auth().signOut();
    }

    getCurrentUser() {
        return this.firebase.auth().currentUser;
    }

    sendEmailVerification() {

        let user = this.getCurrentUser();

        user.sendEmailVerification().then(() => {
            // Email sent.
            console.log('sendEmailVerification');

        }).catch(() => {
            // An error happened.
            console.log('sendEmailVerification error');
        });

    }

    observer(callback) {
        return this.firebase.auth().onAuthStateChanged(callback);
    }

    completeProfile(data) {

        let user = this.getCurrentUser();

        user.updateProfile({
            displayName: data.name,
            photoURL: data.photoURL
        }).then(function () {
            // Update successful.
        }).catch(function (error) {
            // An error happened.
        });
        
    }

}

export default FirebaseApp;