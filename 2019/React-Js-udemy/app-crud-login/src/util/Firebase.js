// import app from 'firebase/app';

const Config = {
    apiKey: "AIzaSyDjgHQGNfe3XcdO79kM54x7TsbLPYOFkj8",
    authDomain: "bracket-54de8.firebaseapp.com",
    databaseURL: "https://bracket-54de8.firebaseio.com",
    projectId: "bracket-54de8",
    storageBucket: "bracket-54de8.appspot.com",
    messagingSenderId: "801313187416"
};

const Firebase = window.firebase.initializeApp(Config);

export default Firebase;