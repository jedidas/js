import React, { Component } from 'react';

class Result extends Component {

    render() {
        return (
            <div className="gran-total">
                <span>${this.props.result}</span>
            </div>
        );
    }

}

export default Result;