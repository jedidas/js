import React, { Component } from 'react';

import Result from './Result';

class Summary extends Component {

    render() {

        console.log('this.props');
        console.log(this.props.data);
        let { brand = '', year = '', plan = '' } = this.props.data;

        if (brand === '' || year === '' || plan === '') {
            return ''
        }

        return (
            <React.Fragment>
                <div className="resumen">
                    <h2>Resumen</h2>
                    <p>Marca: {brand}</p>
                    <p>Año: {year}</p>
                    <p>Plan: {plan}</p>
                </div>
                <Result
                    result={this.props.result}
                />
            </React.Fragment>
        );
    }

}

export default Summary;