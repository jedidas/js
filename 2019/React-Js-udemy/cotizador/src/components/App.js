import React, { Component } from 'react';
import './../css/App.css';

import { obtenerDiferenciaAnio, calcularMarca, obtenerPlan } from './../assets/helper';

import FormQuotes from './FormQuotes';
import Summary from './Summary'

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      result: 0,
      data: {}
    };
  }

  quoteInsuranceMethod = (data) => {

    const { brand, year, plan } = data;
    let result = 2000;
    let difference = obtenerDiferenciaAnio(year);
    let incrementPlan = obtenerPlan(plan);
    result -= ((difference * 3) * result) / 100;
    result = calcularMarca(brand) * result;
    result = parseFloat(incrementPlan * result).toFixed(2);

    this.setState({
      result,
      data
    });

  }

  render() {
    return (
      <div className="quotes-app clearfix">
        <header>
          <h1>Header</h1>
        </header>
        <div className="quotes-body">
          <FormQuotes
            quoteInsuranceMethod={this.quoteInsuranceMethod}
          />
          <Summary
            data={this.state.data}
            result={this.state.result}
          />
        </div>
      </div>
    );
  }
}

export default App;
