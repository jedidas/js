import { createStore } from 'redux'
import reducers from './reducers/'

// const initialState = {};
//Reducers

const store = createStore(
    reducers
);

export default store;