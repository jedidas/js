import React, { Component } from 'react';
import './../css/App.css';

//components
import SongList from './SongList'

class App extends Component {
  render() {
    return (
      <SongList />
    );
  }
}

export default App;
