import React, { Component } from 'react';
import { connect } from 'react-redux';

import SongListItem from './SongListItem'

class SongList extends Component {

    componentDidMount() {

        console.log(
            this.props
        );
        
    }

    render() {
        return (
            <ul className="song-list">

                {this.props.soungs.map((item, key) => {
                    return <SongListItem
                        key={key}
                        indice={key}
                        soung={item}
                    />
                })}

            </ul>
        );
    }

}

//State
const mapStateToProps = (state) => ({
    soungs: state.soungs
})

export default connect(mapStateToProps)(SongList);