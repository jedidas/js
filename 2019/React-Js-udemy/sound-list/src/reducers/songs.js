const songsReducer = () => {
    return [{
            title: 'Title 1',
            duration: '4:05'
        },
        {
            title: 'Title 2',
            duration: '4:05'
        },
        {
            title: 'Title 3',
            duration: '4:05'
        }
    ];
};

export default songsReducer;