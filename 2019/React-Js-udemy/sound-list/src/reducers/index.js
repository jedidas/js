import {
    combineReducers
} from 'redux'

import songs from './songs'
import selectedSongReducer from './selectedSongReducer'

const reducers = combineReducers({
    soungs: songs,
    selectedSoung: selectedSongReducer
});

export default reducers;