
import React, { Component } from 'react';
import PropTypes from 'prop-types'; 

import ListExpense from './ListExpense';

class List extends Component {

    render() {
        return (
            <div className="clearfix">
                <h2>Listado</h2>
                <ul className="list-group">

                    {Object.keys(this.props.expense).map((key) => {
                        return (
                            <ListExpense
                                key={key}
                                item={this.props.expense[key]}
                                removeExpense={this.props.removeExpense}
                                keyItem={key}
                            />
                        )
                    })}

                </ul>
            </div>
        )
    }

}

List.propTypes = {
    expense: PropTypes.object.isRequired
};

export default List;