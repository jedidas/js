
import React, { Component } from 'react';
import PropTypes from 'prop-types'; 

class ListExpense extends Component {

    render() {
        return (
            <li className="list-group-item clearfix">
                {this.props.item.nameExpense}: ¢{this.props.item.countExpense}
                <button onClick={this.props.removeExpense.bind(this, this.props.keyItem)} className="pull-right btn-sm btn btn-danger">X</button>
            </li>
        )
    }

}

ListExpense.propTypes = {
    item: PropTypes.object.isRequired,
    removeExpense: PropTypes.func.isRequired
};

export default ListExpense;