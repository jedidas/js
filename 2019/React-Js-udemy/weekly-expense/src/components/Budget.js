import React, { Component } from 'react';
import PropTypes from 'prop-types'; 

import { revisarPresupuesto } from '../util/helper'

class Budget extends Component {

    render() {
        return (
            <React.Fragment>
                <div className="alert alert-info">Presupuesto: ¢{this.props.budget}</div>
                <div className={revisarPresupuesto(this.props.budget, this.props.remaining)}>Restante: ¢{this.props.remaining}</div>
            </React.Fragment>
        )
    }

}

Budget.propTypes = {
    budget: PropTypes.number.isRequired,
    remaining: PropTypes.number.isRequired
};


export default Budget;