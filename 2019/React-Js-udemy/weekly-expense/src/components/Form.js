import React, { Component } from 'react';
import PropTypes from 'prop-types'; 

class Form extends Component {

    nameExpense = React.createRef();
    countExpense = React.createRef();

    addExpense = (e) => {
        e.preventDefault();

        let state = this.props.addExpense({
            nameExpense: this.nameExpense.current.value,
            countExpense: this.countExpense.current.value
        });
        
        //Reset form
        if(state) e.currentTarget.reset();

    }

    render() {
        return (
            <form onSubmit={this.addExpense}>
            
                <h2>Agrega tus gastos aqui</h2>
                <div className="campo">
                    <label>Nombre Gasto</label>
                    <input className="u-full-width" type="text" placeholder="Ej. Transporte" ref={this.nameExpense} />
                </div>

                <div className="campo">
                    <label>Cantidad</label>
                    <input className="u-full-width" type="number" placeholder="Ej. 300" ref={this.countExpense} />
                </div>

                <input className="button-primary u-full-width" type="submit" value="Agregar" />

            </form>
        )
    }

}


Form.propTypes = {
    addExpense: PropTypes.func.isRequired
};

export default Form;