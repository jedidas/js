import React, {
  Component
} from 'react';
import './../css/App.css';
import './../css/skeleton.css';

import Header from './Header'
import Form from './Form'
import FormBudget from './FormBudget'
import List from './List'
import Budget from './Budget'

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      addBudget: false,
      budget: 0,
      remaining: 0,
      expense: {}
    };

  }

  addBudget = (data) => {

    data = Number(data);

    if (!data && data <= 0) {
      return false;
    }

    let newBudget = {
      ...this.state,
      budget: data,
      remaining: data,
      addBudget: true
    };
    this.setState(newBudget);

    return true;

  }

  addExpense = (data) => {

    if (!data.nameExpense || !data.countExpense) {
      return false;
    }

    let expense = {
      ...this.state.expense
    };

    expense[`expense${Date.now()}`] = data;

    this.subtractExpense(data.countExpense);

    this.setState({
      expense
    });

    return true;

  }

  subtractExpense = (data) => {

    data = Number(data);

    this.setState({
      ...this.state,
      remaining: (this.state.remaining - data)
    });

  }

  sumExpense = (data) => {

    data = Number(data);
    console.log('sumExpense');
    console.log(data);

    this.setState({
      ...this.state,
      remaining: (this.state.remaining + data)
    });

  }

  removeExpense = (key) => {

    let expense = {
      ...this.state.expense
    };
    this.sumExpense(expense[key].countExpense);
    delete expense[key];

    this.setState({
      expense
    });

  }

  render() {
    return (
      <div className="App container">
        <Header />
        <div className="contenido-principal contenido">
          <div className="row">


            <FormBudget
              addBudget={this.addBudget}
              className={this.state.addBudget ? 'hidden' : ''}
            />

            <div className={this.state.addBudget ? 'clearfix' : 'hidden clearfix'}>

              <div className="col-xs-6 one-half colum">
                <Form
                  addExpense={this.addExpense}
                />
              </div>
              <div className="col-xs-6 one-half colum">
                <div className="col-xs-12">
                  <List
                    expense={this.state.expense}
                    removeExpense={this.removeExpense}
                  />
                </div>
              </div>

              <div className="col-xs-12">
                <Budget
                  budget={this.state.budget}
                  remaining={this.state.remaining}
                />
              </div>

            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
