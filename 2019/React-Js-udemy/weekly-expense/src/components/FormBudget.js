import React, { Component } from 'react';
import PropTypes from 'prop-types'; 

class FormBudget extends Component {

    budget = React.createRef();

    addBudget = (e) => {
        e.preventDefault();

        let state = this.props.addBudget(this.budget.current.value);

        if(state) e.currentTarget.reset();

    }

    render() {
        return (
            <form onSubmit={this.addBudget} className={this.props.className + " col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-3"}>
            
            <h2>Agrega presupuesto</h2>

            <div className="campo">
                <label>Presupuesto</label>
                <input className="u-full-width" type="number" placeholder="Ej. 300" ref={this.budget} />
            </div>

            <input className="button-primary u-full-width" type="submit" value="Agregar Presupuesto" />

        </form>
        )
    }
}

FormBudget.propTypes = {
    className: PropTypes.string.isRequired,
    addBudget: PropTypes.func.isRequired
};

export default FormBudget;