import React, { Component } from 'react';
import './css/App.css';

import Products from './components/Products.jsx'
import Header from './components/Header'
import Footer from './components/Footer'

class App extends Component {

  constructor() {
    super();

    this.state = {
      products: [
        { name: 'Original', price: 0 },
      ]
    }

  }

  componentDidMount() {

    setTimeout(() => {

      this.setState({
        products: [
          ...this.state.products,
          { name: 'Libro 1', price: 200 },
          { name: 'Disco Compacto', price: 800 },
          { name: 'Intrumento musical', price: 900 },
          { name: 'Repoductor MP3', price: 100 },
          { name: 'Tv Box', price: 500 },
          { name: 'Herramienta', price: 600 }
        ]
      });

    }, 2500);

  }

  render() {

    return (
      <div className="App">
        <Header />
        <Products products={this.state.products} />
        <Footer />
      </div>
    );
  }
}

export default App;
