import React, { Component } from 'react';

import ProductItem from './ProductItem.jsx'

class Products extends Component {

    render() {

        return (
            <React.Fragment>
                <h2>Lista de Productos</h2>
                <div className="product__list clearfix">
                    {this.props.products.map((item, key) => (
                        <ProductItem
                            product={item}
                            key={key}
                        />
                    ))}
                </div>
            </React.Fragment>
        )
    }

}

export default Products;