import React, { Component } from 'react';

class ProductItem extends Component {

    render() {

        const { name, price } = this.props.product;
        return (
            <div className="product__item col-xs-3">
                <div className="product__item-body">
                    <h3>Product: {name}</h3>
                    <p>Precio: {price}</p>
                </div>
            </div>
        )
    }

}

export default ProductItem;