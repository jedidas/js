import React, { Component } from 'react';
import './../css/App.css';

import News from './News'
import Categories from './Categories'

import { URL, COUNTRY, APIKEY } from './../util/consts'
import { Service } from '../util/Services'

class App extends Component {

  state = {
    news: [],
    category: 'general'
  }

  componentDidMount() {

    this.getNews();

  }

  componentDidUpdate(prevProps, prevState) {

    if (this.state.category !== prevState.category) {
      this.getNews();
    }

  }

  getNews() {
    let promise = Service(`${URL}top-headlines?country=${COUNTRY}&category=${this.state.category}&apiKey=${APIKEY}`);
    promise.then((response) => {

      if (response.hasOwnProperty('articles')) {

        this.setState({
          ...this.state,
          news: response.articles
        });
      }

    })
  }

  changeCategory = (data) => {

    this.setState({
      ...this.state,
      category: data
    });

  }

  render() {
    return (
      <React.Fragment>

        <nav className="App">
          <header className="nav-wrapper light-blue darken-3">
            <span className="brand-logo center">React</span>
          </header>
        </nav>

        <div className="container white contenedor-noticias">
          <Categories
            changeCategory={this.changeCategory}
          />
          <News
            news={this.state.news}
          />
        </div>

      </React.Fragment>

    );
  }
}

export default App;
