import React, { Component } from 'react'

class Categories extends Component {

    categoryRef = React.createRef();
    changeCategory = (e) => {

        e.preventDefault();
        this.props.changeCategory(this.categoryRef.current.value);

    }

    render() {
        return (
            <div className="row">
                <div className="col s12 m8 offset-m2">
                    <form>

                        <div className="input-field col s12">
                            <select ref={this.categoryRef} onChange={this.changeCategory}>
                                <option value="general">General</option>
                                <option value="business">Business</option>
                                <option value="entertainment">Entertainment</option>
                                <option value="health">Health</option>
                                <option value="science">Science</option>
                                <option value="sports">Sports</option>
                                <option value="technology">Technology</option>
                            </select>
                            <label>Seleccione la categoría</label>
                        </div>

                    </form>
                </div>
            </div>
        );
    }
}

export default Categories;