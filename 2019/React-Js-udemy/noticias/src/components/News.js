import React, { Component } from 'react'

import NewsItem from './NewsItem'

class News extends Component {

    render() {
        return (
            <div className="row">
                {this.props.news.map( (newItem, index) => {
                    return (
                        <NewsItem
                            key={index}
                            new={newItem}
                        />
                    )
                } )}
                <NewsItem />
            </div>
        );
    }

}

export default News;