import React, { Component } from 'react'

class NewsItem extends Component {

    render() {

        if (!this.props.hasOwnProperty('new')) {
            return null;
        }

        return (
            <div className="col s12 m6 l4">
                <div className="card">
                    <div className="card-image">
                        <img alt={this.props.new.title} src={this.props.new.urlToImage} />
                        <span className="card-title">{this.props.new.title}</span>
                        <a target="_blank" rel="noopener" className="btn-floating halfway-fab waves-effect waves-light red" href={this.props.new.url}><i className="material-icons">add</i></a>
                    </div>
                    <div className="card-content">
                        <p>{this.props.new.description}</p>
                    </div>
                    <div className="card-action">
                        <a target="_blank"  rel="noreferrer"  href={this.props.new.url}>Leer Más</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default NewsItem;