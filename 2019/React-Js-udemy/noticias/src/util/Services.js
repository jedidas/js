export const Service = (URL) => {

    return fetch(URL).then(function (response) {
        return response.json();
    }).catch(function (error) {
        console.log(error);
    });

}