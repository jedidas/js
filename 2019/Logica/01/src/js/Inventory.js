import Guitar from './Guitar.js';
import GuitarSpec from './GuitarSpec.js';

class Inventory {
  constructor() {
    this.guitars = [];
  }

  addGuitar(serialNumber, price, builder, model, type, backWood, topWood, numStrings) {
    let specs = new GuitarSpec(builder, model, type, backWood, topWood, numStrings);
    let guitar = new Guitar(serialNumber, price, specs);
    this.guitars.push(guitar);
  }

  getGuitar(serialNumber) {
    if (typeof arguments[0] !== 'string') {
      throw new Error('Attribute type is not string');
    }
    for (var i = 0; i < this.guitars.length; i++) {
      if (this.guitars[i].getSerialNumber().trim().toLowerCase() === serialNumber.trim().toLowerCase()) {
        return this.guitars[i];
      }
    }
    return null;
  }

  search(searchSpec) {

    if (searchSpec.__proto__.constructor.name !== 'GuitarSpec') {
      throw new Error('Attribute type is not Guitar');
    }

    let matchingGuitars = [];
    for (var i = 0; i < this.guitars.length; i++) {

      let guitar = this.guitars[i];

      if ( guitar.getSpec().matches(searchSpec) ) {
        matchingGuitars.push(guitar);
      }

    }
    return matchingGuitars;

  }

}

export default Inventory;