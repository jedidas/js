import Guitarspec from './Guitarspec.js';

class Guitar {

  constructor(serialNumber = "", price = 0, specs) {
    if (specs.__proto__.constructor.name !== 'GuitarSpec') {
      throw new Error('Attribute type is not GuitarSpec');
    }
    this.serialNumber = serialNumber;
    this.price = price;
    this.spec = specs;
  };

  getSerialNumber() {
    return this.serialNumber;
  }

  getPrice() {
    return this.price;
  }

  getSpec() {
    return this.spec;
  }

  setSerialNumber(serialNumber) {
    this.serialNumber = serialNumber;
  }

  setPrice(price) {
    this.price = price;
  }

  setSpec(spec) {
    this.spec = spec;
  }

}

export default Guitar;