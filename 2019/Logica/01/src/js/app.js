import Inventory from './Inventory.js';
import GuitarSpec from './GuitarSpec.js';
import {
    Type,
    Builder,
    Wood
} from './enums.js';

let myInventory = new Inventory();

myInventory.addGuitar('V95693', 1499.95, Builder.FENDER, 'STRATOCASTOR', Type.ACOUSTIC, Wood.ALDER, Wood.ALDER, 12);
myInventory.addGuitar('CKSLS', 787457, Builder.FENDER, 'STRATOCASTOR', Type.ACOUSTIC, Wood.ALDER, Wood.ALDER, 12);
myInventory.addGuitar('patitos', 47444, Builder.COLLINGS, 'STRATOCASTOR', Type.ACOUSTIC, Wood.COCOBOLO, Wood.MAPLE, 12);

let iWant = new GuitarSpec(Builder.FENDER, 'STRATOCASTOR', Type.ACOUSTIC, Wood.ALDER, Wood.ALDER, 12);

console.log('Search: ');
console.table( myInventory.search(iWant) );

console.log('getGuitar: ');
console.table( myInventory.getGuitar('CKSLS') );