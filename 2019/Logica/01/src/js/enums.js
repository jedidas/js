var Type = {
  ACOUSTIC: 'acoustic',
  ELECTRIC: 'electric'
};

var Builder = {
  FENDER: 'FENDER',
  MARTIN: 'MARTIN',
  GIBSON: 'gibson',
  COLLINGS: 'collings',
  OLSON: 'olson',
  RYAN: 'ryan',
  PRS: 'prs',
  ANY: 'any'
};

var Wood = {
  INDIAN_ROSEWOOD: 'indian rosewood',
  BRAZILIAN_ROSEWOOD: 'brazilian rosewood',
  MOHOGANY: 'mohogany',
  MAPLE: 'maple',
  COCOBOLO: 'cocobolo',
  CEDAR: 'cedar',
  ADIRONDACK: 'adirondack',
  ALDER: 'alder',
  SITKA: 'sitka',
};

Object.freeze(Type);
Object.freeze(Builder);
Object.freeze(Wood);

export {Type, Builder, Wood};
