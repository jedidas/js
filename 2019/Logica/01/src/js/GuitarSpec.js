class GuitarSpec {

    constructor(builder = "", model = "", type = "", backWood = "", topWood = "", numStrings = 0) {

        this.builder = builder;
        this.model = model;
        this.type = type;
        this.backWood = backWood;
        this.topWood = topWood;
        this.numStrings = numStrings;

    }
    
    getBuilder() {
        return this.builder;
    }

    getModel() {
        return this.model;
    }

    getType() {
        return this.type;
    }

    getBackWood() {
        return this.backWood;
    }

    getTopWood() {
        return this.topWood;
    }

    getNumStrings() {
        return this.numStrings;
    }

    setBuilder(builder) {
        this.builder = builder;
    }

    setModel(model) {
        this.model = model;
    }

    setType(type) {
        this.type = type;
    }

    setBackWood(backWood) {
        this.backWood = backWood;
    }

    setTopWood(topWood) {
        this.topWood = topWood;
    }

    setNumStrings(numStrings) {
        this.numStrings = numStrings;
    }

    matches(otherSpec) {

        if (otherSpec.__proto__.constructor.name !== 'GuitarSpec') {
            throw new Error('Attribute type is not Guitar');
        }

        //Builder
        if (this.builder != otherSpec.builder) {
            return false;
        }
        //Model
        if ((this.model != null) && (this.model != '') && (this.model != otherSpec.model)) {
            return false;
        }
        //Type
        if (this.type != otherSpec.type) {
            return false;
        }
        //CackWood
        if (this.backWood != otherSpec.backWood) {
            return false;
        }
        //topWood
        if (this.topWood != otherSpec.topWood) {
            return false;
        }

        return true;

    }

}

export default GuitarSpec;