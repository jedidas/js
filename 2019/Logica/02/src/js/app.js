import DogDoor from './DogDoor.js';
import Remote from './Remote.js';

var myDoor = new DogDoor();
var remote = new Remote( myDoor );
console.log( remote.pressButton() );
console.log( remote.pressButton() );
