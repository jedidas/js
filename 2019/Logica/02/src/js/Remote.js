class Remote {

    constructor ( newDoor ) {
        this.door = newDoor;
    }

    pressButton () {

        console.log('Pressing the remote control button: ');
        if( this.door.isOpen() ) {
            return this.door.closeDoor() ;
        }else {
            return this.door.openDoor();
        }

    }



}

export default Remote;