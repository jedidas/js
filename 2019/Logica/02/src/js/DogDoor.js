class DogDoor {

    constructor () {
        this.open = false;
    }

    openDoor () {
        return this.open = true;
    }

    closeDoor () {
        return this.open = false;
    }

    isOpen() {
        return this.open;
    }

}

export default DogDoor;