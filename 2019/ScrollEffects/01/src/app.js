$(function () {




    // //Scene
    // var ourScene = new ScrollMagic.Scene({
    //     triggerElement: '#project01',
    //     duration: '900%',
    //     triggerHook: 0.9
    // })
    // .setClassToggle('#project01', 'fade-in')
    // .addIndicators({
    //     name: 'fade scene',
    //     colorTrigger: 'black',
    //     colorStart: '#75C695'
    // })
    // .addTo(controller);


    //Controllers
    var controllerBallon = new ScrollMagic.Controller();
    var controllerNews = new ScrollMagic.Controller();

    var sceneBallon = new ScrollMagic.Scene({
            triggerElement: '#section-velocity',
            offset: 50
        })
        .setPin('#section-velocity')
        .setVelocity('#velocity', {
            opacity: 0.5,
            left: 300
        }, {
            easing: 'linear'
        })
        .addIndicators()
        .addTo(controllerBallon);


    var sceneNews = new ScrollMagic.Scene({
            triggerElement: '#news',
            offset: 50
        })
        .setPin('#news')
        .setVelocity('#news-item', {
            opacity: 0.5,
            left: 300
        }, {
            easing: 'linear'
        })
        .addIndicators()
        .addTo(controllerBallon);

    console.log('ourScene', controllerBallon);



});