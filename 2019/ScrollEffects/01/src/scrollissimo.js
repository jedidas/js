$(document).ready(function () {

    //Items
    var header = document.getElementById('header');
    var screen1 = document.getElementById('screen1');
    var sectionVelocity = document.getElementById('section-velocity');
    var ballonSection = document.getElementById('ballon-section');


    //Creo el Timeline
    var firstTimeline = new TimelineMax({
        paused: true
    });

    TweenLite.defaultEase = Linear.easeNone;

    //Items animation
    // firstTimeline.to(header, 1000, {
    //     scale: 1,
    //     top: '35%'
    // });
    // firstTimeline.to(header, 1000, {
    //     scale: .5,
    //     top: '20%'
    // });

    firstTimeline.to(sectionVelocity, 1000, { rotationX: 0, visibility: 'visible' });

    firstTimeline.to(ballonSection, 1000, { rotationX: 0, visibility: 'visible' });
    
    firstTimeline.to(screen1, 2000, {
        top: '-30%'
    }, '+=1500');

    //Agrego el Timeline a scrollissimo
    scrollissimo.add(firstTimeline, 0, 60);

    $(window).scroll(function () {
        scrollissimo.knock();
    });

});