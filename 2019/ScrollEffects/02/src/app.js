$.fn.wrapEvery = function (options) {

    var settings = $.extend({
        tag: 'span'
    }, options);

    this.each(function (index) {
        var characters = $(this).text().split("");

        $this = $(this);
        $this.empty();
        $.each(characters, function (i, el) {
            $this.append('<' + settings.tag + '>' + el + '</' + settings.tag + '>');
        });

    });
};

$(function () {

    var isMobile = (function (a) {
        return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))
    })(navigator.userAgent || navigator.vendor || window.opera);
    var duration = isMobile ? 3000 : 6000;
    var mainWrapper = document.getElementsByTagName('main')[0];
    var player1Item = document.querySelector('#player1');
    var player2Item = document.querySelector('#player2');
    var mobileItem = document.querySelector('#mobile');
    var mobileServicesSection = document.querySelector('#mobile-services-section');


    function generateBubble(bubbleItems, increment, orientation) {
        var bubble = [];
        var from = {
            top: (mobileServicesSection.offsetHeight / 2),
            scale: 0.7,
            opacity: 0
        };
        var to = {
            scale: 1,
            opacity: 1,
            ease: Power4.easeOut
        };
        for (var index = 0; index < bubbleItems.length; index++) {

            from[orientation] = (window.innerWidth / 2) - ($(bubbleItems[index]).width() / 2);
            to[orientation] = (window.innerWidth / 2) - mobileItem.offsetWidth;
            to['top'] = (mobileItem.offsetHeight / 4) + ((index > 0) ? (increment * index) : 0);
            bubble.push(TweenMax.fromTo(bubbleItems[index], 3, jQuery.extend(true, {}, from), jQuery.extend(true, {}, to)));

        }
        return bubble;
    }

    // Controllers
    var controller1 = new ScrollMagic.Controller({
        globalSceneOptions: {
            // set trigger hook to top of viewport
            triggerHook: 0
        }
    });

    //Paths
    var pathBall = [{
            x: -(document.querySelector('#ball').offsetWidth),
            y: -150
        }, {
            x: (window.innerWidth / 4),
            y: -190
        },
        {
            x: (window.innerWidth / 2), //(window.innerWidth / 2) - (player2Item.offsetWidth - 210),
            y: -174
        }
    ];


    //Animations

    $("#title-nfl").wrapEvery();
    var textNFL = TweenMax.staggerTo("#title-nfl span", 0.2, {
        opacity: 1,
        top: 0,
        ease: Elastic.easeOut.config(1, 0.3),
        y: -500
    }, 0.2);

    var player1 = TweenMax.fromTo(player1Item, 1, {
        left: -(player1Item.offsetWidth),
        opacity: 0
    }, {
        left: isMobile ? ((window.innerWidth / 2) - (player1Item.offsetWidth / 2)) : (window.innerWidth / 6),
        opacity: 1,
        delay: 0.5
    });

    var player1B = TweenMax.fromTo(player1Item, 1, {
        left: isMobile ? ((window.innerWidth / 2) - (player1Item.offsetWidth / 2)) : (window.innerWidth / 6),
        opacity: 1
    }, {
        left: -(player1Item.offsetWidth),
        opacity: 0,
        delay: 0.5
    });

    var ball = TweenMax.to("#ball", 1, {
        bezier: {
            autoRotate: 180,
            curviness: 0.3,
            values: pathBall
        },
        ease: Power2.easeInOut
    });

    var player2 = TweenMax.fromTo(player2Item, 1, {
        right: -(player2Item.offsetWidth + player2Item.offsetWidth),
        scale: 0.7,
    }, {
        scale: 1.3,
        right: ((window.innerWidth / 2) - player2Item.offsetWidth + (document.querySelector('#ball').offsetWidth / 2)),
        ease: Circ.easeInOut,
    });

    //Scene 2
    var mobile = TweenMax.fromTo(mobileItem, 1, {
        bottom: -50,
        scale: 0.8,
    }, {
        scale: 1,
        bottom: 0,
        opacity: 1,
        ease: Circ.easeInOut,
    });
    var bubbleLeftItems = document.querySelectorAll('.bubble-left');
    var bubbleRightItems = document.querySelectorAll('.bubble-right');
    var bubbleLeft = generateBubble(bubbleLeftItems, 200, 'left');
    var bubbleRight = generateBubble(bubbleRightItems, 200, 'right');

    //Timelines
    var timeLine1 = new TimelineMax().add([
            textNFL,
            player1,
        ]).add([
            player1B,
        ])
        .add([
            ball, player2
        ]);

    var timeLine2 = new TimelineMax().add([
            mobile
        ])
        .add([].concat(bubbleLeft).concat(bubbleRight));



    // Scenes
    var scene1 = new ScrollMagic.Scene({
            triggerElement: "#player-section",
            triggerHook: 0,
            duration: (duration - window.innerHeight)
        })
        .setPin('#player-section')
        .setTween([timeLine1])
        .addIndicators() // add indicators (requires plugin)
        .addIndicators()
        .addTo(controller1);

    var scene2 = new ScrollMagic.Scene({
            triggerElement: mobileServicesSection,
            triggerHook: 0,
            duration: (duration - window.innerHeight)
        })
        .setPin(mobileServicesSection)
        .setTween([timeLine2])
        .addIndicators()
        .addTo(controller1);




    $(".js-scroll-to").on('click', function (event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 2000);
    });

    var sections = $('.section-page');
    var nav = $('nav.nav-site');

    $(window).on('scroll', function () {

        var cur_pos = $(this).scrollTop();

        sections.each(function () {

            var top = $(this).offset().top;
            var bottom = top + $(this).outerHeight();

            if (cur_pos >= top && cur_pos <= bottom) {

                nav.find('a').removeClass('active');
                sections.not($(this)).removeClass('active');

                $(this).addClass('active');
                nav.find('a[href="#' + $(this).attr('id') + '"]').addClass('active');

            }
        });

    });

});