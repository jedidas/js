const config = {
  width: 370,
  height: 550,
  parent: 'container',
  type: Phaser.AUTO,
  backgroundColor: '#fff',
  scene: [Menu, MainScene, Over],
  physics: {
    default: 'arcade',
    arcade: {
      gravity: {
        y: 50
      }
    }
  }
};

var game = new Phaser.Game(config);

function preload() {}

function create() {}

function update(time, delta) {}
