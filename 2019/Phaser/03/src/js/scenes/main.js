class MainScene extends Phaser.Scene {

  constructor() {
    super({key: 'MainScene'});
  }

  preload() {

    console.log('main');
    this.load.image('background', '/assets/images/background-day.png');
    this.load.image('pipe', '/assets/images/tubo.png');

    //avatar
    this.load.spritesheet('player', './assets/images/pagaros.png', {
      frameWidth: 36,
      frameHeight: 26
    }); //Cargo sprite del player

  }

  create() {

    this.background = this.add.tileSprite(0, 0, 370, 550, 'background').setOrigin(0, 0); //Agrego el fondo

    //player
    this.player = this.physics.add.sprite(34, 24, 'player').setFrame(1);
    this.player.body.gravity.y = 1200; //Aumento la gravedad

    //Animacion
    this.anims.create({
      key: 'fly',
      frames: this.anims.generateFrameNumbers('player', {
        start: 0,
        end: 2
      }),
      frameRate: 15,
      repeat: -1
    });
    this.player.anims.play('fly', true);

    //Controles
    this.jump = this.input.keyboard.createCursorKeys();

    //Tubos
    this.pipes = this.physics.add.group({
      enableBody: true,
      key: 'pipe',
      repeat: 20,
      checkWorldBounds: true,
      outOfBoundsKill: true
    });
    // this.pipes.enableBody = true;
    // this.pipes.createMultiple(20, 'pipe');
    // console.log( this.pipes );

    //
    // this.timer = this.time.events.loop(1500, this.makeColumn);
    this.time.addEvent({delay: 1500, loop: true, callback: this.makeColumn, callbackScope: this});

    console.log('getChildren', this.pipes.getChildren()[0]);

  }

  makeColumn() {

    console.log('makeColumn');

    var space = Math.floor(Math.random() * 5) + 1;
    for (var i = 0; i < 8; i++) {
      if ((i != space) && (i != space + 1)) {
        this.makePipes(370, (i * 55 + 20))
      }
    }

  }

  makePipes(x, y) {
    // var pipe = this.pipes.getFirstDead(true);
    var pipe = this.pipes.getFirstDead(true);
    pipe.body.gravity.x = 0;
    // pipe.disableBody(true, true);

    // pipe.setActive﻿(true).body.reset(x, y);
    // pipe.enableBody(true, x, y, true, true)

    pipe.setOrigin(0.5, 1);
    pipe.body.reset(x, y);
    pipe.body.velocity.x = -180;
    // pipe.tilePositionX += -180;
    pipe.checkWorldBounds = true;
    pipe.outOfBoundsKill = true;
    console.log('pipe ', pipe );
  }

  jumping() {

    //velocidad y posicion del avatar
    this.player.tilePositionX += 0.7;
    this.player.body.velocity.y = -350;

    //animacion de inclinacion
    this.tweens.add({targets: this.player, angle: -20, duration: 100});

  }

  update(time, delta) {

    // this.pipes.getChildren()[0].tilePositionX += 3;

    if (this.jump.space.isDown) {
      this.jumping();
    }

    if (this.player.angle <= 20) {
      this.player.angle += 1;
    }

    if ((this.player.body.position.y > game.config.height) || (this.player.body.position.y < 0) && game.gaming) {
      console.log('Murio');
      // this.cameras.main.shake(500);
    } else {
      this.background.tilePositionX += 3;
      console.log('Vivo');
    }

  }

}
