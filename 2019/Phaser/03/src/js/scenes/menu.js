class Menu extends Phaser.Scene {

  constructor() {
    super({key: 'Menu'});
  }

  preload() {

    this.load.image('btnPlay', '/assets/images/btn.png')

  }

  create() {

    var graphics = this.add.graphics();
    graphics.fillStyle(0xffffff, 1);
    graphics.fillRect(0, 0, 370, 550);

    this.btnPlay = this.add.sprite((game.config.width / 2), (game.config.height / 2), 'btnPlay').setInteractive();
    this.btnPlay.on('pointerdown', () => {

      //Cambiar de escena
      this.scene.start('MainScene');
      

    }); // Start game on click.

    //
    this.add.text((game.config.width / 2), (game.config.height / 2 + 60), 'Iniciar', {
      font: "bold 40px Open Sans, sans-serift",
      fill: '#19a74d',
      align: 'center'
    }).setOrigin(0.5, 0);

  }

  update(time, delta) {}

}
