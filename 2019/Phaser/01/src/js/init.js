var gameOptions = {

  // player gravity
  playerGravity: 1900,

  // player horizontal speed
  playerSpeed: 200,

  // player force
  playerJump: 600,

  // enemy horizontal speed
  enemySpeed: 150
}

const config = {
  width: 320 * 2,
  height: 180 * 2,
  parent: 'container',
  type: Phaser.AUTO,
  scene: {
    preload: preload,
    create: create,
    update: update
  },
  physics: {
    default: 'arcade',
    arcade: {
      gravity: {
        y: 500
      }
    }
  }
};

var game = new Phaser.Game(config);

function preload() {
  console.log('preload');

  // this.load.image('player', './assets/spr_f_traveler_idle_anim.gif');
  this.load.spritesheet('avatar', './assets/dude.png', {
    frameWidth: 32,
    frameHeight: 48
  });

}

function create() {

  this.player = this.physics.add.sprite(50, 50, 'avatar');
  this.player.setFrame(4);

  // this.player.setRotation(0); Rotacion inicial
  // this.player.setOrigin(0.5, 1); Cordenadas de rotacion
  this.player.setCollideWorldBounds(true); //Colision con los limites del mundo
  this.player.setBounce(0.4);
  // this.player.setAcceletation(50, 0); Aceleracion incremental
  // this.player.setVelocity(50, 0);

  // setting hero gravity
  this.player.body.gravity.y = gameOptions.playerGravity;

  // setting hero horizontal speed
  // this.player.body.velocity.x = gameOptions.playerSpeed;
  // the hero can jump
  this.canJump = true;
  this.walk = false;

  /*

  */
  //La camara siga a nuestro personaje
  // this.cameras.main.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
  // this.cameras.main.startFollow(this.player);
  console.log( this.cameras.main );

  /*
  Animation
  */

  this.anims.create({
    key: 'left',
    frames: this.anims.generateFrameNumbers('avatar', {
      start: 0,
      end: 3
    }),
    frameRate: 10,
    repeat: -1
  });

  this.anims.create({
    key: 'turn',
    frames: [
      {
        key: 'avatar',
        frame: 4
      }
    ],
    frameRate: 20
  });

  this.anims.create({
    key: 'right',
    frames: this.anims.generateFrameNumbers('avatar', {
      start: 5,
      end: 8
    }),
    frameRate: 10,
    repeat: -1
  });

  /*
  CONTROLLERS
  */
  this.input.keyboard.on('keydown_LEFT', () => {
    if (this.walk) {
      this.player.x = this.player.x - 5;
      this.player.anims.play('left', true);
    }
  });
  this.input.keyboard.on('keydown_RIGHT', () => {
    this.player.x = this.player.x + 5;
    this.player.anims.play('right', true);
  });
  this.input.keyboard.on("keydown_UP", () => {

    // console.log('this.player.body.blocked.down', this.player.body.blocked.down);

    if ((this.player.body.blocked.down)) {
      this.player.body.velocity.y = -gameOptions.playerJump;
      // hero can't jump anymore
      this.canJump = false;
    }

  });
  this.input.keyboard.on('keyup_LEFT', () => {
    this.player.anims.play('turn');
    this.canJump = true;
  });
  this.input.keyboard.on('keyup_RIGHT', () => {
    this.player.anims.play('turn');
    this.canJump = true;
  });

}

function update(time, delta) {

  if (this.player.x < 200 && !this.walk) {
    this.player.x++;
    // this.player.angle++;
  } else {
    this.walk = true;
  }

}
