
window.addEventListener('load', function() {

	var game = new Phaser.Game({
        "title": "Game1",
        "width": 800,
        "height": 450,
        "type": Phaser.AUTO,
        "backgroundColor": "#88F"
	});
	game.scene.add("Boot", Boot, true);
	
});

class Boot extends Phaser.Scene {

	preload() {
		this.load.pack("section1", "assets/pack.json");
	}

	create() {
		this.scene.start("Level");
	}

}
