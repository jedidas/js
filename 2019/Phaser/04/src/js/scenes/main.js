class MainScene extends Phaser.Scene {

  constructor() {
    super({key: 'MainScene'});
  }

  preload() {

    this.load.image('sky', 'assets/img/sky.png');
    this.load.image('ground', 'assets/img/platform.png');
    this.load.image('star', 'assets/img/star.png');
    this.load.image('bomb', 'assets/img/bomb.png');
    this.load.spritesheet('dude', 'assets/img/dude.png', {
      frameWidth: 32,
      frameHeight: 48
    });

  }

  create() {

    this.add.image(400, 300, 'sky');
    // this.add.image(400, 300, 'star');

    //
    this.platforms = this.physics.add.staticGroup();
    this.platforms.create(400, 568, 'ground').setScale(2).refreshBody();

    this.platforms.create(500, 400, 'ground');
    this.platforms.create(50, 250, 'ground');
    this.platforms.create(750, 220, 'ground');

    this.player = this.physics.add.sprite(100, 450, 'dude');
    this.player.setBounce(0.2);
    this.player.setCollideWorldBounds(true);
    this.player.body.setGravityY(300);

    this.anims.create({
      key: 'left',
      frames: this.anims.generateFrameNumbers('dude', {
        start: 0,
        end: 3
      }),
      frameRate: 10,
      repeat: -1
    });

    this.anims.create({
      key: 'turn',
      frames: [
        {
          key: 'dude',
          frame: 4
        }
      ],
      frameRate: 20
    });

    this.anims.create({
      key: 'right',
      frames: this.anims.generateFrameNumbers('dude', {
        start: 5,
        end: 8
      }),
      frameRate: 10,
      repeat: -1,
      scale: {
        x: 0.5,
        y: 0.5
      }
    });

    ///
    this.physics.add.collider(this.player, this.platforms);

    //
    this.stars = this.physics.add.group({
      key: 'star',
      repeat: 11,
      setXY: {
        x: 12,
        y: 0,
        stepX: 70
      }
    });

    this.stars.children.iterate(function(child) {

      child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8)).setScale(0.2, 0.2);

    });
    this.physics.add.collider(this.stars, this.platforms);
    this.physics.add.overlap(this.player, this.stars, this.collectStar, null, this);

    //
    this.bombs = this.physics.add.group();
    this.physics.add.collider(this.bombs, this.platforms);
    this.physics.add.collider(this.player, this.bombs, this.hitBomb, null, this);

  }

  hitBomb(player, bomb) {

    this.physics.pause();

    player.setTint(0xff0000);

    player.anims.play('turn');

    this.gameOver = true;
    this.scene.start('Menu');

  }

  collectStar(player, star) {
    star.disableBody(true, true);

    if (this.stars.countActive(true) === 0) {

      this.stars.children.iterate(function(child) {
        child.enableBody(true, child.x, 0, true, true);
      });

      var x = (this.player.x < 400)
        ? Phaser.Math.Between(400, 800)
        : Phaser.Math.Between(0, 400);

      var bomb = this.bombs.create(x, 16, 'bomb');
      bomb.setScale(0.2, 0.2);
      bomb.setBounce(1);
      bomb.setCollideWorldBounds(true);
      bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);
      bomb.allowGravity = false;

    }

  }

  controllers() {

    this.cursors = this.input.keyboard.createCursorKeys();

    if (this.cursors.left.isDown) {

      this.player.setVelocityX(-160);
      this.player.anims.play('left', true);

    } else if (this.cursors.right.isDown) {

      this.player.setVelocityX(160);
      this.player.anims.play('right', true);

    } else {

      this.player.setVelocityX(0);
      this.player.anims.play('turn');

    }

    if (this.cursors.up.isDown && this.player.body.touching.down) {
      console.log('up');
      this.player.setVelocityY(-530);
    }
  }

  update(time, delta) {

    //
    this.controllers();

  }

}
