class MainScene extends Phaser.Scene {

  constructor() {
    super({key: 'MainScene'});
  }

  preload() {
    console.log('MainScene');
    //Cargo fondo
    this.load.image('background', '/assets/images/background-day.png');
    this.load.image('base', '/assets/images/base.png');
    //controles
    this.load.image('btnPlay', '/assets/images/btn.png');
    //avatar
    this.load.spritesheet('player', './assets/images/pagaros.png', {
      frameWidth: 36,
      frameHeight: 26
    }); //Cargo sprite del player
    // this.load.image('player', '/assets/images/yellowbird-downflap.png');
  }

  create() {

    this.background = this.add.tileSprite(0, 0, 370, 550, 'background').setOrigin(0, 0); //Agrego el fondo

    //this.base = this.add.tileSprite(0, 550, 370, 112, 'base').setOrigin(0, 1); //Agrego el el suelo
    //platforms
    this.platforms = this.physics.add.staticGroup();
    this.platforms.create(50, 550, 'base').setScale(2 ,1).refreshBody();

    this.player = this.physics.add.sprite(34, 24, 'player').setFrame(1);
    this.player.setCollideWorldBounds(true); //Colision con los limites del mundo
    this.player.setBounce(0.4);

    //
    this.physics.add.collider(this.player, this.platforms);

    //Animacion
    this.anims.create({
      key: 'fly',
      frames: this.anims.generateFrameNumbers('player', {
        start: 0,
        end: 2
      }),
      frameRate: 15,
      repeat: -1
    });
    this.player.anims.play('fly', true);

    //Controles
    this.cursors = this.input.keyboard.createCursorKeys();

  }

  update(time, delta) {
    this.background.tilePositionX += 0.7; //Muevo el background
    // this.base.tilePositionX += 1.4;

    //Controles
    if (this.cursors.left.isDown) {
      console.log('left');
      console.log(this.player.setVelocityX);
      //this.player.setVelocityX(160);
    }

  }

}
