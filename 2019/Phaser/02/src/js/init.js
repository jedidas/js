const config = {
  width: 370,
  height: 550,
  parent: 'container',
  type: Phaser.AUTO,
  scene: [MainScene],
  physics: {
    default: 'arcade',
    arcade: {
      gravity: {
        y: 100
      }
    }
  }
};

var game = new Phaser.Game(config);

function preload() {}

function create() {}

function update(time, delta) {}
