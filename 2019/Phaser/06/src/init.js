;
(function() {
  let player;
  let cursors;
  let bird;
  const config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    parent: 'container',
    physics: {
      debug: true,
      default: 'arcade',
      arcade: {
        gravity: {
          y: 0
        }
      }
    },
    scene: {
      preload,
      create,
      update
    }
  };

  const game = new Phaser.Game(config);

  function preload() {

    // Runs once, loads up assets like images and audio
    this.load.image('tiles', '/assets/img/tuxmon-sample-32px-extruded.png');
    this.load.tilemapTiledJSON('map', '/jsons/new-place.json');

    //player
    this.load.atlas("atlas", '/assets/img/atlas.png', '/jsons/atlas.json');

    //Bird
    this.load.atlas('bird', '/assets/img/spritesheet-bird.png', '/jsons/spritesheet-bird.json');

  }

  function create() {

    const map = this.make.tilemap({key: 'map'});

    //cargar el tilemap
    const tileset = map.addTilesetImage("tuxmon-sample-32px-extruded", "tiles"); //Primer parametro nombre de la imagen, segundo parametro el key delo load image
    const decoration1 = map.createStaticLayer('Below Player', tileset, 0, 0);
    const worldLayer = map.createStaticLayer('World', tileset, 0, 0);
    const aboveLayer = map.createStaticLayer('Above Player', tileset, 0, 0);

    //Colisiones
    worldLayer.setCollisionByProperty({collides: true});

    const debugGraphics = this.add.graphics().setAlpha(0);
    worldLayer.renderDebug(debugGraphics, {
      tileColor: null, // Color of non-colliding tiles
      collidingTileColor: new Phaser.Display.Color(243, 134, 48, 255), // Color of colliding tiles
      faceColor: new Phaser.Display.Color(40, 39, 37, 255) // Color of colliding face edges
    });

    aboveLayer.setDepth(10);

    //Player
    const spawnPoint = map.findObject("Objects", obj => obj.name === "Spawn Point");
    player = this.physics.add.sprite(spawnPoint.x, spawnPoint.y, "atlas", "misa-front").setSize(30, 40).setOffset(0, 24);

    bird = this.physics.add.sprite(spawnPoint.x, spawnPoint.y, "bird", "bluebird.001").setSize(30, 40).setOffset(0, 24);

    //Camara default
    const camera = this.cameras.main;

    //Controles
    cursors = this.input.keyboard.createCursorKeys();

    // Set up the arrows to control the camera
    // controls = new Phaser.Cameras.Controls.FixedKeyControl({
    //   camera: camera,
    //   left: cursors.left,
    //   right: cursors.right,
    //   up: cursors.up,
    //   down: cursors.down,
    //   speed: 0.5
    // });
    camera.startFollow(player);

    // Watch the player and worldLayer for collisions, for the duration of the scene:
    this.physics.add.collider(player, worldLayer);

    // Constrain the camera so that it isn't allowed to move outside the width/height of tilemap
    camera.setBounds(0, 0, map.widthInPixels, map.heightInPixels);

    // Create the player's walking animations from the texture atlas. These are stored in the global
    // animation manager so any sprite can access them.
    const anims = this.anims;
    anims.create({
      key: "misa-left-walk",
      frames: anims.generateFrameNames("atlas", {
        prefix: "misa-left-walk.",
        start: 0,
        end: 3,
        zeroPad: 3
      }),
      frameRate: 10,
      repeat: -1
    });
    anims.create({
      key: "misa-right-walk",
      frames: anims.generateFrameNames("atlas", {
        prefix: "misa-right-walk.",
        start: 0,
        end: 3,
        zeroPad: 3
      }),
      frameRate: 10,
      repeat: -1
    });
    anims.create({
      key: "misa-front-walk",
      frames: anims.generateFrameNames("atlas", {
        prefix: "misa-front-walk.",
        start: 0,
        end: 3,
        zeroPad: 3
      }),
      frameRate: 10,
      repeat: -1
    });
    anims.create({
      key: "misa-back-walk",
      frames: anims.generateFrameNames("atlas", {
        prefix: "misa-back-walk.",
        start: 0,
        end: 3,
        zeroPad: 3
      }),
      frameRate: 10,
      repeat: -1
    });

    anims.create({
      key: "bird-fly",
      frames: anims.generateFrameNames("bird", {
        prefix: "bluebird.",
        start: 0,
        end: 2,
        zeroPad: 3
      }),
      frameRate: 10,
      repeat: -1
    });

    // Debug graphics
    this.input.keyboard.once("keydown_D", event => {

      console.log('keydown_D');
      // Turn on physics debugging to show player's hitbox
      this.physics.world.createDebugGraphic();

      // Create worldLayer collision graphic above the player, but below the help text
      const graphics = this.add.graphics().setAlpha(0.5).setDepth(20);
      worldLayer.renderDebug(graphics, {
        tileColor: null, // Color of non-colliding tiles
        collidingTileColor: new Phaser.Display.Color(243, 134, 48, 255), // Color of colliding tiles
        faceColor: new Phaser.Display.Color(40, 39, 37, 255) // Color of colliding face edges
      });
    });

  }

  function update(time, delta) {

    bird.anims.play('bird-fly', true);
    // Runs once per frame for the duration of the scene
    // controls.update(delta);
    const speed = 175;
    const prevVelocity = player.body.velocity.clone();

    // Stop any previous movement from the last frame
    player.body.setVelocity(0);

    // Horizontal movement
    if (cursors.left.isDown) {
      player.body.setVelocityX(-speed);
    } else if (cursors.right.isDown) {
      player.body.setVelocityX(speed);
    }

    // Vertical movement
    if (cursors.up.isDown) {
      player.body.setVelocityY(-speed);
    } else if (cursors.down.isDown) {
      player.body.setVelocityY(speed);
    }

    // Normalize and scale the velocity so that player can't move faster along a diagonal
    player.body.velocity.normalize().scale(speed);

    // Update the animation last and give left/right animations precedence over up/down animations
    if (cursors.left.isDown) {
      player.anims.play("misa-left-walk", true);
    } else if (cursors.right.isDown) {
      player.anims.play("misa-right-walk", true);
    } else if (cursors.up.isDown) {
      player.anims.play("misa-back-walk", true);
    } else if (cursors.down.isDown) {
      player.anims.play("misa-front-walk", true);
    } else {

      player.anims.stop();

      // If we were moving, pick and idle frame to use
      if (prevVelocity.x < 0)
        player.setTexture("atlas", "misa-left");
      else if (prevVelocity.x > 0)
        player.setTexture("atlas", "misa-right");
      else if (prevVelocity.y < 0)
        player.setTexture("atlas", "misa-back");
      else if (prevVelocity.y > 0)
        player.setTexture("atlas", "misa-front");
      }

  }

}());
