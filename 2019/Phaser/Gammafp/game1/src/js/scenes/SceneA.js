class SceneA extends Phaser.Scene {

    constructor() {

        super({
            key: "SceneA"
        });

    }

    //preload
    preload() {

        this.load.image('pajaro', './assets/img/bird.png');
        this.load.image('pajaro2', './assets/img/bird_dos.png');

    }

    generateItem(x, y, img) {
        let item = this.physics.add.image(x, y, img);
        item.setOrigin(0.5);

        //physics
        item.setCollideWorldBounds(true);
        item.setAcceleration(0, 100);
        item.setVelocity(0, 100);
        return item;
    }

    create() {

        this.temporalItem = null;

        this.currentItem = this.generateItem((this.scale.width / 2), -100, 'pajaro');
        
        //contollers
        this.cursor = this.input.keyboard.createCursorKeys();

        


    }

    update(time, delta) {

        // console.log(time, delta);
        if (this.cursor.right.isDown) {
            console.log('right');
            this.currentItem.x = this.currentItem.x + 3;
        }
        if (this.cursor.left.isDown) {
            console.log('left');
            this.currentItem.x = this.currentItem.x - 3;
        }

        if (this.cursor.up.isDown) {
            console.log('up');
            this.currentItem.angle = this.currentItem.angle + 90;
        }
        

        if(this.currentItem.y+(this.currentItem.height/2) >= this.scale.baseSize.height) {

            this.oldItem = this.currentItem;
            this.currentItem = this.generateItem((this.scale.width / 2), -100, 'pajaro2');
            this.physics.add.collider(this.oldItem, this.currentItem,()=>{  console.log('pajaro collider'); }, null, this);

        }
        
        

    }

}


export default SceneA;