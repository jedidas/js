import SceneA from './scenes/SceneA.js';

const physicsConfig = {
    default: 'arcade',
    arcade: {
        debug: true,
        gravity: {
            y: 0
        }
    }
};

const config = {
    width: 400,
    height: 450,
    parent: "container",
    type: Phaser.AUTO,
    physics: physicsConfig,
    scene: [SceneA]
};

var game = new Phaser.Game(config);

