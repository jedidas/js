class MainScene extends Phaser.Scene {

  constructor() {
    super({
      key: 'MainScene',
      physics: {
        default: 'arcade',
        arcade: {
          gravity: {
            y: 800,
            debug: true
          }
        },
        matter: {
          debug: true,
          gravity: {
            y: 15
          }
        }
      }
    });
    this.map;
    this.player;
    this.cursors;
    this.groundlayer;
    this.text;
  }

  preload() {

    this.load.json("sprite_physics", "assets/jsons/world1.json");

    this.load.tilemapTiledJSON('map', '/assets/jsons/map.json');
    this.load.spritesheet('tiles', 'assets/img/tiles.png', {
      frameWidth: 70,
      frameHeight: 70
    });
    this.load.image('coin', 'assets/img/coinGold.png');
    this.load.atlas('player', 'assets/img/player.png', 'assets/jsons/player.json');
  }

  create() {



    this.map = this.make.tilemap({key: 'map'});
    // tiles for the ground layer
    var groundTiles = this.map.addTilesetImage('tiles');
    // create the ground layer
    this.groundLayer = this.map.createDynamicLayer('World', groundTiles, 0, 0);
    // the player will collide with this layer
    this.groundLayer.setCollisionByExclusion([-1]);

    // Set colliding tiles before converting the layer to Matter bodies - same as we've done before
    // with AP. See post #1 for more on setCollisionByProperty.
    this.groundLayer.setCollisionByProperty({ collides: true });

    // Get the layers registered with Matter. Any colliding tiles will be given a Matter body. We
    // haven't mapped out custom collision shapes in Tiled so each colliding tile will get a default
    // rectangle body (similar to AP).
    this.matter.world.convertTilemapLayer(this.groundLayer);//Colision

    // Visualize all the matter bodies in the world. Note: this will be slow so go ahead and comment
    // it out after you've seen what the bodies look like.
    // this.matter.world.createDebugGraphic();



    // set the boundaries of our game world
    // this.physics.world.bounds.width = this.groundLayer.width;
    // this.physics.world.bounds.height = this.groundLayer.height;
    this.scene = this;
    this.player = this.matter.add.sprite(600, 200, "player", 0);
    const { Body, Bodies } = Phaser.Physics.Matter.Matter; // Native Matter modules
    const { width: w, height: h } = this.player;
    console.log( w, h );
    const mainBody = Bodies.rectangle(0, 0, w * 0.6, h, { chamfer: { radius: 10 } });
    this.sensors = {
      bottom: Bodies.rectangle(0, h * 0.7, w * 0.25, 2, { isSensor: true }),
      left: Bodies.rectangle(-w * 0.35, 0, 2, h * 0.5, { isSensor: true }),
      right: Bodies.rectangle(w * 0.35, 0, 2, h * 0.5, { isSensor: true })
    };
    const compoundBody = Body.create({
      parts: [mainBody, this.sensors.bottom, this.sensors.left, this.sensors.right],
      frictionStatic: 0,
      frictionAir: 0.02,
      friction: 0.1
    });

    // create the player sprite
    // this.player = this.matter.add.sprite(100, 200, 'player');
    // this.player.setCircle(this.player.width / 2, { restitution: 1, friction: 0.25 });
    // console.log( this.player );
    this.player
      .setExistingBody(compoundBody)
      .setScale(0.9)
      .setFixedRotation() // Sets inertia to infinity so the player can't rotate
      .setPosition(300, 200);
    console.log( this.player.body );
    // this.physics.add.collider(this.groundLayer, this.player);
    // this.player.setCollideWorldBounds(true); // don't go out of the map
    // this.physics.add.collider(this.groundLayer, this.player);
    // (this.groundLayer, this.player);

    this.camera();

  }

  camera() {
    // set bounds so the camera won't go outside the game world
    this.cameras.main.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels);
    // make the camera follow the player
    this.cameras.main.startFollow(this.player);

    // set background color, so the sky is not black
    this.cameras.main.setBackgroundColor('#ccccff');
  }

  controllers() {

    var moveForce = 0.01;
    var cursors = this.input.keyboard.createCursorKeys();

    if (cursors.left.isDown) { // if the left arrow key is down
      this.player.setFlipX(true);
      // this.player.body.setVelocityX(-200); // move left
      // this.player.setFlipX(true);
      this.player.applyForce({ x: -moveForce, y: 0 });

    } else if (cursors.right.isDown) { // if the right arrow key is down
      this.player.setFlipX(false);
      // this.player.body.setVelocityX(200); // move right
      // this.player.setFlipX(true);
      this.player.applyForce({ x: moveForce, y: 0 });

    } else {
      // this.player.setVelocityX(0);
      // this.player.setFlipX(true);
      this.player.applyForce({ x: 0, y: 0 });

    }
    // if ((cursors.space.isDown || cursors.up.isDown) && this.player.body.onFloor()) {
    if ( cursors.space.isDown || cursors.up.isDown ) {
      // this.player.body.setVelocityY(-500); // jump up
      this.player.setVelocityY(-11);
    }
  }

  update(time, delta) {

    //
    this.controllers();

  }

}
