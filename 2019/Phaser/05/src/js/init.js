const config = {
  width: 800,
  height: 600,
  parent: 'container',
  type: Phaser.AUTO,
  backgroundColor: '#fff',
  pixelArt: false,
  scene: [
    MainScene, Menu, Over
  ],
  physics: {
    default: 'arcade',
    arcade: {
      gravity: {
        y: 800,
        debug: true
      }
    }
  }
};

var game = new Phaser.Game(config);

function preload() {}

function create() {}

function update(time, delta) {}
