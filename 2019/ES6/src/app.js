function sum(...args) {
  let total = args.reduce((accumulator, currentValue, currentIndex, array) => {
    //console.log( accumulator, currentValue, currentIndex, array );
    return accumulator + currentValue;
  }, 0);

  console.log('reduce en argument array: ', total);
}

sum('David', 1, 2, 3, 4, 8, 7, 9, 6, 5);
sum(1, 3);

//VAR
var x = 3;

function func(randomize) {
  if (randomize) {
    var x = Math.random(); // (A) scope: whole function
    return x;
  }
  return x; // accesses the x from line A
}
console.log(func(false)); // undefined

//LET
let y = 5;

function funcy(randomize) {
  if (randomize) {
    let y = Math.random();
    return y;
  }
  return y;
}
console.log(funcy(false), funcy(3)); //5

//BLOCK
/*
  Las variables dentro del BLOCK son privadas
*/
{
  let privateVar = "Soy privada";
  console.log(privateVar);
}
//console.log(privateVar); // Uncaught ReferenceError: privateVar is not defined

//TEMPLATE LITERALS
function printCoord(x, y) {
  return `(${x}, ${y})`;
}

console.log(`TEMPLATE LITERALS ${printCoord(1, 2)}`);

//MULTIPLE LINES
const HTML5_SKELETON = `
<!doctype html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
  </body>
</html>`;
console.log('MULTIPLE LINES', HTML5_SKELETON);


//ARROW FUNCTIONS
let AppArrowFunction = x => {

  var privateVar = "Soy privada";
  return {
    get: () => privateVar
  };

}
console.log('AppArrowFunction', AppArrowFunction().get());

//Error detienen la ejecucion
// throw new Error('Error menssage: ');

// MULTIPLE RETURN VALUES

//via arrays
const [, YEAR, MONTH, DAY, ALGO, AQUI] = [2018, 10, 21, {}, 'officina', 789, 777];
console.log(YEAR, MONTH, DAY, ALGO, AQUI);

//via objects
const obj = {
  foo: 123
};
const {
  writable,
  configurable
} = Object.getOwnPropertyDescriptor(obj, 'foo');

console.log(writable, configurable); // true true


//
const arr = ['a', 'b', 'c'];
for (const elem of arr) {
  console.log(elem);
}

console.log('arr.entries: ', arr.entries());

for (const [index, elem] of arr.entries()) {
  console.log(index, elem);
}


var objDestructuring = {
  x: 5,
  y: 20,
  //z: 3
};

function mult({
  x,
  y,
  z = 4
}) {
  /*
  Es posible definir un valor por defecto para el Destructuring
  */
  return x * y * z;
}

console.log('Destructuring', mult(objDestructuring));


var arrDestructuring = [1, 2, 3, 4, 5, 6, 7];

var [a, b, ...c] = arrDestructuring
/*
Es posible asignar valores a variables desde un array
y asignar el resto del array a la ultima variable
*/
console.log('Destructuring asignar el resto de los elementos de un array', a, b, c);


var [a, b, c, d, arrSpreadOperators] = [
  [1, 2],
  [4, 5],
  [8, 9, 10], 11
];

arrSpreadOperators = [0, ...a, 3, ...b, 6, 7, ...c, d];
/*
Es posible conbinar varios arreglos y valores en un solo arraglo
*/
console.log('Spread Operators: ', arrSpreadOperators);



//Copy array
var a = [1,2,3];
// var b = a.slice(); //ES5
var b = [...a];

b.push('New Item array');

console.log(a, b);
