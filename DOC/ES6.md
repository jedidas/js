
Practica de ES6
===

const
------
Crea una constante que puede ser global o local a la función en la cual es declarada. Las constantes siguen las mismas reglas de ámbito que las variables.
Las constantes deben inicializarse apenas son declaradas.

```javascript
    var x; // x === undefined
    const z; // SyntaxError: const declarations must have an initializer
	//Forma correcta
    const y = 10; // y === 10
```

Una vez que una constante ha sido declarada, no se puede cambiar la referencia ni el literal que tiene asignado, aunque sí podemos hacer algunas cosas:

```javascript
    const sabrosas = { frutas: ['manzana', 'naranja', 'pera'] }
    sabrosas = {}
    // sabrosas es de solo lectura
    sabrosas.frutas.push('plátano')
    console.log(sabrosas);
    // { frutas: ['manzana', 'naranja', 'pera', 'plátano'] }
```

let
------
La sentencia let declara una variable de alcance local, la cual, opcionalmente, puede ser inicializada con algún valor.
Lef declara una variable cuyo scope es local y no es elevada en el hoisting, no puede ser accedida desde otro ambito esto evita colisión de varaibles

```javascript
    for (let i = 0; i < 10; i++) {
      console.log(i + 1);
    }

    // i no existe aquí

    for (let i = 0; i < 20; i++) {
      console.log(i + 1);
    }
```

**La sentencia const y let no son elevados en el hoisting.**


Map
=========

El objecto Map es un sencillo mapa clave/valor. Cualquier valor (tanto objetos como valores primitivos) pueden ser usados como clave o valor.

**myMap.set('key', 'Value')**
------

Agrega un nuevo valor al Map

```javascript
myMap.set('name', 'David');
myMap.set('country', 'Costa Rica');
myMap.set('temporal', 'Este valor va a ser borrado');
console.log(myMap);
```

**myMap.clear()**
------

Limpia el objeto Map



**myMap.get('key')**
------


El atributo get consulta por un valor especifico del objeto

```javascript
console.log('get: ', myMap.get('name'));
```

**myMap.size**
------


Atributo size devuelve el numero de elmentos

```javascript
console.log('size: ', myMap.size);
```

**myMap.delete('key')**
------


Atributo delete remuevo el objeto apartir de su llave

```javascript
console.log('delete: ', myMap.delete('temporal'));
```


**myMap.entries()**
------


Retorna un nuevo map con arreglos que corresponden a la llave valor ingresados en el set

```javascript
console.log('entries() ',myMap.entries());
```


**myMap.has("key")**
------


Consulta si existe una llave especifica en el Map y retorna un boleano

```javascript
console.log('has("key") ', myMap.has("country") );
```

**myMap.keys()**
------


Devuelve un objeto con las lleves de los diferentes valores

```javascript
let  mapIterator = myMap.keys();
console.log('keys() ', mapIterator);
```

**myMap.values()**
------

Devuelve un objeto con los valores de las llaves.

```javascript

console.log('values() ', myMap.values() );
console.log('Objeto ',myMap);

```

**myMap. forEach(Value, Index, Map)**
------

Recive 3 parametros y permite iterrar por los valores del Map

```javascript

console.log('forEach(Value, Index, Map)');
myMap.forEach(function(Value, Index, Map){

  console.log('Value: ', Value);

  console.log('Index: ', Index);

  console.log('Map: ', Map);

});

```

**myMap.clear()**
------

Limpia el objeto Map

```javascript
myMap.clear();
console.log('clear(): ', myMap);

```
