'use strict';
(function (window){


var ObjetoEstatico = {};

Object.defineProperties(ObjetoEstatico, {
	nombre: {
		writable: true,
		value: 'David',
		enumerable: true
	},
	apellido: {
		value: 'Mora Arroyo',
		writable: false,
		enumerable: true
	},
	edad: {
		value: 28,
		enumerable: false
	},
	saludo: {
		get: function () {
			return 'Hola ' + this.nombre + ' ' + this.apellido;
		},
		set: function (nombre) {
			this.nombre = nombre;
		}
	}

});

//Este valor no puede sobre escribirse al estar como writable: false, por defecto los valores definidos con defineProperties son writable: false
//ObjetoEstatico.apellido = 'Carballo';
console.log( ObjetoEstatico );
console.log( ObjetoEstatico.saludo );

//Sobre escribo nombre
ObjetoEstatico.saludo = 'Maria';
console.log( ObjetoEstatico );


//For in para recorrer los valores de un objeto
for (var key in ObjetoEstatico) {
	console.log( ObjetoEstatico[key] );
}

//Factory
function crearObjeto (nombre, apellido, edad) {
	return {
		nombre: nombre,
		apellido: apellido,
		edad: edad
	};
}

var nuevoUsuario = crearObjeto('Manolo', 'Herrera', 45),
	nuevoUsuario2 = crearObjeto.call(null, 'Alberto', 'Jose', 22);

console.log( nuevoUsuario, nuevoUsuario2 );


var Gustos = {
	programa: [],
	set: function (nombre) {
		this.programa.push(nombre);
		return this;
	},
	get: function (){
		return this.programa;
	}
};


//Constructor
function Persona (nombre, apellido, edad, nodo) {
	this.nombre = nombre;
	this.apellido = apellido;
	this.nodo = document.getElementById(nodo) ;
	this.edad = edad;
}
Persona.prototype.saludo = function () {
	this.nodo.innerHTML =  'Hola ' + this.nombre + ' ' + this.apellido;
	return this;
};

function AccionesPersona () {

	this.cumplirAnos = function () {
		this.edad++;
		return this.edad;
	}
	this.nombreCompleto = function () {
		this.nombre + ' ' + this.apellido;
	}

}

var usuarioNuevo = new Persona('Manolo', 'Herrera', 45, 'usuarioNuevo'),
	usuarioNuevo2 =  new Persona('Alberto', 'Jose', 22, 'usuarioNuevo2');


console.log("usuarioNuevo, usuarioNuevo2");
console.log( usuarioNuevo.saludo() );
usuarioNuevo.saludo();
usuarioNuevo2.saludo();


function Empleado (nombre, apellido, edad, nodo, ocupacion, nombrePrograma) {
	this._this = this;
	this.ocupacion = ocupacion;
	this.empresa = {
		nombre: 'LumenUp',
		direccion: 'Barrio escalante'
	}
	Persona.call(this, nombre, apellido, edad, nodo);
	// Gustos.series.call(this, nombrePrograma);
}
Empleado.prototype.comoLlegar = function (ubicacion) {
	return 'Como llegar desde ' + ubicacion + ' hasta ' + this.empresa.direccion;
};
Empleado.prototype.gustos = Gustos;


function Jefe (nombre, apellido, edad, nodo, ocupacion, nombrePrograma) {
	Empleado.call(this);
	AccionesPersona.call(this);
}



var Yo = new Empleado('Jedidias', 'Legacy', 35, 'jedidias', 'Front End');

Yo.gustos.set('Video Juegos');
Yo.gustos.set('Cine');
Yo.gustos.set('TV');



console.log( Yo.comoLlegar('San Jose centro') );
console.log( Yo.gustos.get() );

console.log( Yo );



var myJefe = new Jefe('Boss', 'Forever', 15, 'jedidias', 'Master Ship');
myJefe.cumplirAnos();
console.log( myJefe );

console.log( myJefe.edad );
myJefe.cumplirAnos()

console.log( myJefe.edad );
myJefe.cumplirAnos()

console.log( myJefe.edad );
myJefe.cumplirAnos()


}(window))