'use strict';

//Objetos
var ObjetoLiteral = {
    name: 'David',
    apellido: 'Mora',
    saludo: function() {
        return this.name + ' ' + this.apellido;
    }
};

//Contructor de objetos
function CrearLiteral(name, apellido) {
    return {
        name: {
            value: 'David',
            writable: true
        },
        apellido: apellido || '',
        saludo: function() {
            return this.name + ' ' + this.apellido;
        }
    };
}

var objeto = new CrearLiteral('David', 'Mora');
var objetoDos = new CrearLiteral('Fabian', 'Arroyo');
var objetoTres = new CrearLiteral();
var objetoCuatro = new CrearLiteral();
var objetoEstatico = {};
console.log(objeto, objetoDos, objetoTres);



objetoCuatro['factory-controller'] = function() {
    console.log('Este es un metodo con un nombre en String');
};
console.log(objetoCuatro);









//Objetos estaticos
//writable en false, desactiva la edicion del objeto, por defecto writable es false

Object.defineProperties(objetoEstatico, {
    nombre: {
        value: 'Alberto',
        writable: true
    },
    apellido: {
        value: 'Jhonson',
        writable: true
    },
    saludo: {
        get: function() {
            return this.nombre + ' ' + this.apellido;
        },
        set: function(value) {
            this.nombre = value.nombre;
            this.apellido = value.apellido;
        }
    }
});
console.log("objetoEstatico", objetoEstatico);







function Persona (nombre, apellido, nodo) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.nodo = document.getElementById(nodo);
    Persona.instancia++;
}

Persona.prototype.saludo = function () {
    return this.nombre + ' ' + this.apellido;
};

Persona.instancia = 0;
//Nueva persona
var Vecino = new Persona('German', 'Aleman', 'german');
console.log( 'Vecino' );
console.log( Vecino );

//
function Empleado (nombre, apellido, nodo, cargo) {
    this.cargo = cargo;
    Persona.call(this, nombre, apellido, nodo);
};

var empleado1 = new Empleado('Olman', 'Sosa', 'olman', 'Maestro de obras');
console.log( 'empleado1' );
console.log( empleado1 );


function Recurso (nombre, apellido, cargo) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.cargo = cargo;
};
Recurso.prototype = new Persona();

var recurso1 = new Recurso('Lidia', 'Arroyo', 'Ama de casa');
console.log( 'recurso1' );
console.log( recurso1 );

