'use strict';

var App = {};
console.log(App.constructor);
console.log(App.constructor === Object);

//NameSpace
(function (myApp) {

  //Valores
  var categoryA = ['David', 'Maria'];
  var categoryB = ['Isaac'];

  //Metodo de mostrar
  myApp.get = function (category) {
    if (category === 'A') {
      return categoryA;
    }
    if (category === 'B') {
      return categoryB;
    }
  };

  //Medoto de enviar valores
  myApp.set = function (category, value) {

    if (category === 'A') {
      categoryA.push(value);
      return category + ':' + value;
    }
    if (category === 'B') {
      categoryB.push(value);
      return category + ':' + value;
    }

  };

  //Agrega al principio del array
  myApp.unshift = function (category, value) {

    if (category === 'A') {
      categoryA.unshift(value);
      return category + ':' + value;
    }
    if (category === 'B') {
      categoryB.unshift(value);
      return category + ':' + value;
    }

  };

  //Concatena valores
  myApp.concat = function () {
    return categoryA.concat(categoryB);
  };

  //Junta todos los valores
  myApp.join = function (separator) {
    return this.concat().join(separator);
  };

  //Invierte el orden del array
  myApp.reverse = function (category) {
    if (category === 'A') {
      return categoryA.reverse();
    }
    if (category === 'B') {
      return categoryB.reverse();
    }
    return this.concat().reverse();
  };

  //Ordena numerico y alfabeticamente
  myApp.sort = function (category) {
    if (category === 'A') {
      return categoryA.sort();
    }
    if (category === 'B') {
      return categoryB.sort();
    }
    return this.concat().sort();
  };
  //Devuelve la posicion de la primer coinsidencia
  myApp.indexOf = function (category, value) {
    if (category === 'A') {
      return categoryA.indexOf(value);
    }
    if (category === 'B') {
      return categoryB.indexOf(value);
    }
    value = category;
    return this.concat(value).indexOf(value);
  };

  //Devulve la posicion de la ultima coincidencia
  myApp.lastIndexOf = function (category, value) {
    if (category === 'A') {
      return categoryA.lastIndexOf(value);
    }
    if (category === 'B') {
      return categoryB.lastIndexOf(value);
    }
    value = category;
    return this.concat(value).lastIndexOf(value);
  };

  //Quito el untimo valor
  myApp.pop = function (category) {
    if (category === 'A') {
      return categoryA.pop();
    }
    if (category === 'B') {
      return categoryB.pop();
    }
  };
  //Quito el primer valor
  myApp.shift = function (category) {
    if (category === 'A') {
      return categoryA.shift();
    }
    if (category === 'B') {
      return categoryB.shift();
    }
  };
  myApp.splice = function (category, value) {
    var position = null;
    position = this.indexOf(category, value);
    if (position !== -1) {
      this.get(category).splice(position, 1);
    }
    try {
      return this.get(category);
    } finally {
      position = null;
    }
  };
  //Convierto a mayuscula
  myApp.toUpperCase = function (category, value) {
    return (this.get(category)[this.indexOf(category, value)] !== undefined) ? this.get(category)[this.indexOf(category, value)].toUpperCase() : false;
  };
  //Convierto a minusculas
  myApp.toLowerCase = function (category, value) {
    return (this.get(category)[this.indexOf(category, value)] !== undefined) ? this.get(category)[this.indexOf(category, value)].toLowerCase() : false;
  };


}(App));