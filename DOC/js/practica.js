"use strict";


/*Operadores Logicos*/

//Operador OR ||
/*Si el valor que se esta evaluando a la izquierda del doble pai || es falso retorna el siguiente valor y si el valor evaluado es verdadero retorna este valor es retornado*/
var estado = '';
var valor = undefined;
var algo = estado || valor || 'Final';
console.log(algo);


//Operador AND &&
/*Si el valor que se esta evaluando a la izquierda del doble && es falso retorna ese valor y si el valor evaluado es verdadero retorna el siguiente valor.*/
var estado = 'asda';
var valor = false;
var algo = estado && valor && 'Final';
console.log(algo);


/*
Arreglos
*/

//Ejemplo
var arreglo1 = ["David", 'Fabian'];
var arreglo2 = ['Mora', 'Arroyo'];
var arreglo3 = ['Mora', undefined, 'Arreglo', 15, 'JOSE', false, 'David', 22, 'andres', 1, 'Carlos', 8, 'Mora', 'Mora'];
var arreglo4 = ['Matias', 'Arreglo', 15, 'JOSE', false, 'David', 22, 'andres', 'Carlos', 8, 'Mora', 'Santiago', 'Juan'];
var vector = [arreglo1, arreglo3];
console.log(arreglo3);


//Concatena dos arreglos
// concat()
// 
var arregloConcat = arreglo1.concat(arreglo2);
console.log("concat");
console.log(arregloConcat);


//Convierte un arreglo a string y lo devuelve separado por comas.
// toString()
console.log("toString");
console.log(arreglo3.toString());


console.log("toLocaleString");
console.log(arreglo3.toLocaleString());





// join();
//Convierte arreglos en String, y los separa segun el parametro del metodo


//Ejemplo

//Separa el arreglo por -
var juntos = arregloConcat.join('-');
console.log("join");
console.log(juntos);



// split();
//Convierte un string a array buscando en el string un valor y separando el texto por ese valor

//Ejemplo
var separado = juntos.split('-');
console.log("split");
console.log(separado);







//sort()

//alterna el orden de los valores del arreglo los reordena numericamente y alfabeticamente
//El orden es primer los numeros, despues nos textos en Mayuscula, despues los textos en minuscula y los false e undefined al final

var ArrayOrdenado = arreglo3.sort();
console.log("sort");
console.log(ArrayOrdenado);

console.log("vector");
console.log(vector);
var orderVector = vector.sort();
console.log("orderVector");
console.log(orderVector);



//reverse()
//Invierte el orden de los valores del arreglo

var ArrayReverse = ArrayOrdenado.reverse();
console.log("reverse");
console.log(ArrayReverse);




//indexOf()
/*
Busca x dentro del array y devuelve la posición de la primera ocurrencia.
Si el valor no existe retorna -1
Si el valor existe mas de una vez retorna la primer coinsidencia.
*/

//Ejemplo
console.log('indexOf');
console.log(arreglo3.indexOf('Mora'));


//lastIndexOf()
/*
Busca x dentro del array y devuelve la posición de la ultima ocurrencia.
*/

//Ejemplo
console.log('lastIndexOf');
console.log(arreglo3);
console.log(arreglo3.lastIndexOf('Mora'));
console.log(arreglo3[arreglo3.lastIndexOf('Mora')]);



console.log(arreglo4);
//pop()
/*
Quita el último item de la matriz y lo devuelve como resultado.
*/

//Ejemplo
console.log('pop');
console.log(arreglo4.pop());
console.log(arreglo4);


//shift()
/*
Quita el primer item de la matriz y lo devuelve como resultado.
*/

//Ejemplo
console.log('shift');
console.log(arreglo4.shift());
console.log(arreglo4);



//push()
/*
Push agrega uno o más items al final de la matriz.
*/

//Ejemplo
console.log('push');
console.log(arreglo4.push('Nuevo'));
console.log(arreglo4);


//unshift()
/*
Antepone items al comienzo de la matriz.
*/

//Ejemplo
console.log('unshift');
console.log(arreglo4.unshift('Va de primero'));
console.log(arreglo4);



//slice()
/*
Devuelve una sub-matriz de un array. en si devuelve un array tomando valores de otro array
*/

//Ejemplo
console.log('slice');
console.log(arreglo4.slice(2, 5));




//toUpperCase()
/*
Convierte un string a mayuscula
*/

//Ejemplo
var texto = 'david fabian mora arroyo';
console.log('toUpperCase()');
var mayusculas = texto.toUpperCase();
console.log(mayusculas);


//toLowerCase()
/*
Convierte un string a mniculas
*/

//Ejemplo
console.log('toLowerCase()');
console.log(mayusculas.toLowerCase());

//splice(index, howMany);
/*
Permite seleccionar una sub matriz dentro de un arreglo y editarlo ya sea eliminadolo o remplazandolo
El metodo splice() cambia el contenido de un array removiendo elementos existentes y/o agregando nuevos elementos.
*/

//Ejemplo
console.log('splice');
console.log(arreglo4.splice(1, 3));
console.log(arreglo4);




/*METODOS NUEVOS DE LOS ARRAYs*/



//every();
/*
Determina si todos los miembros de una matriz satisfacen la prueba especificada.
El metodo every ejecuta la funcion custom que se le pasa y le pasa cada elemento del array, esta funcion puede recivir 3 parametros, elemento del array, indice del array, el array en si
Se utiliza para verificar cada valor pasandolo por la verificacion y todos retornan TRUE en la verificacion every retorna true
Si algun valor no comple la verificacion every retorna false
*/


//every()
// ejemplo
function VerificaSiEsMayor(element, index, array) {
    return element >= 10;
}
console.log("every");
console.log([12, 5, 8, 130, 44].every(VerificaSiEsMayor)); // false
console.log([12, 54, 18, 130, 44].every(VerificaSiEsMayor)); // true






//some();
/*
Determina si todos los miembros de una matriz satisfacen la prueba especificada.
El metodo every ejecuta la funcion custom que se le pasa y le pasa cada elemento del array, esta funcion puede recivir 3 parametros, elemento del array, indice del array, el array en si
Se utiliza para verificar cada valor pasandolo por la verificacion y todos retornan TRUE en la verificacion every retorna true
Si algun valor no comple la verificacion every retorna false
*/


//some()
// ejemplo
function isBiggerThan10(element, index, array) {
    return element > 10;
}
console.log("some");
console.log([2, 5, 8, 1, 4].some(isBiggerThan10)); // false
console.log([12, 5, 8, 1, 4].some(isBiggerThan10)); // true




//filter()
/*
Filter() crea un nuevo arreglo con todos los elementos que pasan la prueba implementada por la función prevista.
*/

//Ejemplo
function isBigEnough(element) {
    return element <= 10;
}
var filtered = [12, 5, 8, 130, 44, 1].filter(isBigEnough);
console.log('filter');
//Devuelve todos los numeros que son menos a 10
console.log(filtered);



//forEach
/*
Ejecuta determinada función una vez por cada elemento del arreglo.
*/

//Ejemplo
console.log("forEach");

function logArrayElements(element, index, array) {
    console.log("a[" + index + "] = " + element);
}
[2, 5, 9].forEach(logArrayElements);




//map
/*
retorna un nuevo array con cada uno de los valores procesados en la funcion.
Sirve para modificar o editar los valores dentro del arreglo
*/

//Ejemplo
console.log('map');

function mapear(element, index, array) {
    return element;
}
console.log(arreglo4.map(mapear));

//
var numbers = [1, 4, 9];
var doubles = numbers.map(function(num) {
    return num * 2;
});
// doubles is now [2, 8, 18]. numbers is still [1, 4, 9]





//reduce
/*
Devuelve la suma o la concatenacion de todos los valores procesados en el arreglo 
 */

console.log("reduce");

// Define the callback function.
function appendCurrent(previousValue, currentValue) {
    return previousValue + "::" + currentValue;
}

// Create an array.
var elements = ["abc", "def", 123, 456];

// Call the reduce method, which calls the callback function
// for each array element.
var result = elements.reduce(appendCurrent);
console.log(result);

//Resultado
//  abc::def::123::456




//arguments
//===
/*
Es un metodo de las funciones que permite saber los argumentos que se el pasan a una funcion como parametros.
Se comporta como un array aunque NO es un array y no posee los metodos de Array(), solo posee length
*/


console.log("arguments");
//Ejemplo
function argumentos() {

    console.log(arguments);
    console.log('Cantidadd', arguments.length);
    for (var prop in arguments) {
        console.log(arguments[prop]);
    }


}
argumentos('Hola', true, {
    state: 'ok'
});


//arguments.callee
/*
Es un metodo de arguments que permite llamar a su misma funcion contenedora, pero solo si no se usa "use strict", ya que este restringe el acceso a estos metodos
*/

//Ejemplo
function argumentosCallee() {

    // arguments.callee();

}

argumentosCallee('Nada');







//Objetos







