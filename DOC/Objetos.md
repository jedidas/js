
Herencia de prototipos
===

Los objetos en javascript heredan de otros objetos, un objeto mantiene una relacion con el objeto padre del cual hereda de manera que el nuevo objeto solo contiene lo que lo hace diferente.

Object.defineProperties
===

Permite definir una serie de atributos y metodos de un objeto literal, este método permite tener un control más avanzado de cómo se podrá manipular la propiedad.

Al definir cada objeto se esta manera se puede configurar si el objeto es sobre escribible o si se puede listar al recorrerlo en eun for in.

Cuando se define una propiedad con **Object.defineProperty**, por defecto no es ni enumerable ni configurable ni grabable(excepto en Chrome, donde por defecto sí es grabable).


configurable
------
Determina si el objeto puede ser cambiado y si la propiedad se puede eliminar desde el objeto correspondiente

enumerable
------
Determina si se puede listar al recorrerlo en en for in si esta en false el metodo no se reconoce en el ciclo. Por defecto su valor es false,


writable
------
Define si el objeto se puede sobre escribir, Por defecto su valor es true

```javascript
	//Ejemplo
	var ObjetoEstatico = {};

	Object.defineProperties(ObjetoEstatico, {
		nombre: {
			writable: true,
			value: 'David',
			enumerable: true
		},
		apellido: {
			value: 'Mora Arroyo',
			writable: false,
			enumerable: true
		},
		edad: {
			value: 28,
			enumerable: false
		},
		saludo: {
			get: function () {
				return 'Hola ' + this.nombre + ' ' + this.apellido;
			},
			set: function (nombre) {
				this.nombre = nombre;
			}
		}

	});
	
	ObjetoEstatico.saludo; //Retorna Hola David
	ObjetoEstatico.saludo = 'Jedidas'; //Retorna Hola Jedidas

------

for in
===
Permite recorrer todas los metodos de un objeto literal, la variable Key corresponde al nombre del metodo o atributo del objeto

```javascript
	for (var key in ObjetoEstatico) {
		console.log( objeto[key] );
	}
```



Objetos Estaticos
===

Object.freeze
------

Congela un objeto literal: es decir, previene que nuevas propiedades sean agregadas; privene que las propiedades existentes sean eliminadas; y previene que las propiedades existentes, o su capacidad de enumeración, configuración, o escritura. de ser cambiadas

```javascript
var obj = {
	name: 'David'
};

Object.freeze(obj);
obj.foo = 'sparky'; // arroja un TypeError

console.log(Object.isFrozen(obj));
//Devuelve true

obj.foo = 'sparky';
// arroja un TypeError ya que no se puede modificar

```



Instancia de Objetos
===
El operador **instanceof** devuelve verdadero si el objeto especificado es del tipo especificado

```javascript
console.log( 'String' instanceof String ); //Reforna false

console.log( 123 instanceof Number ); //Reforna false

console.log( 123 instanceof Object ); //Reforna true

console.log( {} instanceof Object ); //Reforna true

```

Los strings y los numeros retornan false ya que instanceof crea una nueva isntancia del texto o el numero por debajo y al hacer la comparacion la instancia apunta a un objeto diferente