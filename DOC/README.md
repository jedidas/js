Esta es una practiva basica de Javascript
===

Esta es una practica para estudiar el diseño de patrones en javascript.


[Markdown Cheatsheet reference](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)



NameSpace
================

Este patron permite crear un modulo y agregarles metodos y propiedades.
Manteniendo la logica separada del modulo creado
Al pasar el objeto windows como this como un argumento se puede acceder mas rapidamente al mismo siendo mas eficiente.

Creo un objeto nameSpace
===============

```javascript
var MyNamespace = {};
(function(global, $, nameSpace) {

    //Variables privadas
    var a = null,
        b = 'Este es un nameSpace',
        c = null,
        d = null;

    //Metodos privados
    a = function(message) {
        global.console.log(message);
    };
    c = function() {
        return d();
    };
    d = function(message) {
        return b + ' ' + (message || 'y es genial');
    };

    //Propeidades del modulo que son accesibles
    nameSpace.metodo1 = a;
    nameSpace.metodo2 = c;
    nameSpace.metodo3 = d;
    //Asi Jquery es parte del nameSpace
    nameSpace.$ = $;

})(this, jQuery, MyNamespace);

console.log('\n NameSpace Patterns \n');

//Llamo a uno de los metodos del objeto
console.log(MyNamespace.metodo3());
```




 Closures 
================

```javascript
var clausura = function() {
    var userName = null,
        password = null;
    return {
        getUser: function() {
            return userName;
        },
        setUser: function(newVal) {
            userName = newVal ? newVal : userName;
        }
    }
}
//Este Closures puede ser fabricado Cuantas veces sea necesario
var nodo = new clausasura();
//Se puede inicializar la propiedad o metodo
nodo.setUser('Maria');
//Se puede consultar la propiedad
nodo.getUser()
"Maria"

```




Singleton
================

Son objetos unicos, que no pueden ser duplicados

```javascript
var Singleton = (function() {

    var instance = null,
        state = null;
    var init = function(id, nombre) {
        return {
            id: id,
            nombre: nombre
        }
    }

    getInstance = function() {
        //Retorna el valor
        return instance;
    };
    setInstance = function(id, nombre) {
        //Si instance es false lo inicializa de manera que se inicializa solo una vez
        if (!instance) {
            instance = init(id, nombre);
            state = true;
        } else {
            //Al tratar de inicializar el metodo nuevamente este retorna false
            state = false;
        }
        return state;
    };

    return {
        getInstance: getInstance,
        setInstance: setInstance
    }

})();

var mySingleton = Singleton;
console.log('\n Singleton Patterns \n');
console.log(mySingleton.setInstance(7, 'David'));

```



Factory
========

Factory se encarga de la creación de objetos 

```javascript
function Factory() {
    //Este metodo crea los objetos
    this.createEmployee = function(type) {
        var employee;

        if (type === "fulltime") {
            employee = new FullTime();
        } else if (type === "parttime") {
            employee = new PartTime();
        } else if (type === "temporary") {
            employee = new Temporary();
        } else if (type === "contractor") {
            employee = new Contractor();
        }

        employee.type = type;
        employee.say = function() {
            log.add(this.type + ": rate " + this.hourly + "/hour");
        }
        return employee;
    }

}

//Tipos de objetos
var FullTime = function() {
    this.hourly = "$12";
};
var PartTime = function() {
    this.hourly = "$11";
};
var Temporary = function() {
    this.hourly = "$10";
};
var Contractor = function() {
    this.hourly = "$15";
};


// log helper, muestra contenido
var log = (function() {
    var log = '';
    return {
        add: function(msg) {
            log += msg + "\n";
        },
        show: function() {
            console.log(log);
            log = "";
        }
    }
})();

```

Este metodo imprimie los valores de los factoryes

```javascript

function run() {

    var employees = [],
        factory = new Factory();


    //Creo los objetos en el array
    employees.push(factory.createEmployee("fulltime"));
    employees.push(factory.createEmployee("parttime"));
    employees.push(factory.createEmployee("temporary"));
    employees.push(factory.createEmployee("contractor"));


    //Recorro el array
    for (var i = 0, len = employees.length; i < len; i++) {
        employees[i].say();
    }


    //Muestro el arreglo
    log.show();
}
run()

```

Creamos los objetos que van a ser creados en el factory

```javascript
// Creamos nuestras clases que crearemos con el Factory
function Perro(params) {
    // setup o valores por default
    this.color = params.color || "Negro";
    this.raza = params.raza || "Labrador";
    this.nombre = params.nombre || "Buddy";
}

function Gato(params) {
    this.color = params.color || "Blanco";
    this.cazador = params.cazador || true;
    this.nombre = params.nombre || "Misu";
}

```



Factory abstracto
==========

No necesita conoces los metodos que van a ser fabricados

```javascript



var AbstractAnimalFactory = (function() {
    // almacen de clases registradas
    var clasesRegistradas = {};

    function createAnimal(tipo, params) {

        //console.log(clasesRegistradas);

        var clase = clasesRegistradas[tipo];
        if (clase)
            return new clase(params);
        else
            return null;
    };

    function registerClass(tipo, clase) {
        clasesRegistradas[tipo] = clase;
        return clasesRegistradas; //AbstractAnimalFactory;
    }

    return {
        createAnimal: createAnimal,
        registerClass: registerClass
    };
})();

```

Creamos los objetos que van a ser creados en el factory

```javascript

// Registramos la clase Gato
AbstractAnimalFactory.registerClass("gato", Gato);

// instanciamos un gatito
var minino = AbstractAnimalFactory.createAnimal("gato", {
    nombre: "Chelsea"
});

console.log('\n Factory Patterns \n');
console.log(minino);

```






Observer pattern
===============
Este patron crea un observador que se puede ejecutar cuando hay un cambio en el codigo o en el flujo de trabajo de la aplicacion. Ej: al termninar una llamada ajax.

```javascript

*/
//Creo un objeto que registra 
var observable = {};

//Creo una funcion auto ejecutable y le paso el objeto creado para configurarlo y agregarle metodos y atributos. Todo lo que le pae al argumento le pasa al objeto del parametro
(function(obser) {

    //Objeto privado que almacenara las suscripciones, los listteners al evento
    var suscripciones = {},
        indiceSuscribir = -1;


    //Metodo para suscribir otros objetos o usuarios a la notificacion
    obser.suscribir = function(suscripcionNombre, funcion) {
        //Si el tema al que se quieren requistrar no existe se crea el tema como un objeto vacio
        if (!suscripciones[suscripcionNombre]) {
            suscripciones[suscripcionNombre] = [];
        }

        /*
        Cada vez que se registra un listtener o un objeto o usuario a un evento se crea un ID indiceSuscribir paso de ser Cero a 1

        En si se crean temas que son el indice del objeto suscripciones y a este objeto que es un array se le agregan nuevos suscriptores
        u objetos suscriptores, identificados por un un token y un metodo propio del objeto suscriptor, para que al realizar una publicacion
        se busque el tema de la suscripciones y se recorran los objetos suscritos y se ejecute su metodo de notificasion

        */
        var token = (++indiceSuscribir).toString();
        suscripciones[suscripcionNombre].push({
            token: token,
            //Funcion que realiza una accion en el objeto para realiar una accion
            notificacion: funcion
        });
        return token;


    };



    /*
    Metodo para publicar o notificar a los otros objetos que ha ocurrido un cambio
        Se le pasan 2 parametros
        El primero es el nombre del 
    */
    obser.publicar = function(suscripcionNombre, argumento) {

        //Si no existen temas o suscripciones con este indice no se hace nada
        if (!suscripciones[suscripcionNombre]) {
            return false;;
        }

        var suscriptores = suscripciones[suscripcionNombre],
            /*Si existen suscriptores (suscriptores es verdadero) a contador le asigno el length si no es 0
                En si se cuenta cuantos objetos o usuarios estan suscritos a esta noticia 
             */
            contador = suscriptores ? suscriptores.length : 0;

        while (contador--) {
            /*
            Recorremos todos los objetos o usuarios suscritos a esta suscripcion por medio de su token y llamamos el metodo de notificacion.
            Ademas a la notificasion le pasamos el nombre de la suscripcion y el valor
            */
            suscriptores[contador].notificacion(suscripcionNombre, argumento);
        }

        //Retorno la funcion de publicar
        return this;


    };

    /*
    Este metodo borra a un usuario u objeto de la lista de suscripciones de un tema especifico, atraves de su token
    */
    obser.desuscribir = function(token) {
        /*
        Se recorren todos los temas en las suscripciones
        */
        for (tema in suscripciones) {
            /*
            Si el tema no esta vacio o sea si no es un array vacio sin suscriptores
            */
            if (suscripciones[tema]) {
                /*
                Recorro todos los usuarios u objetos suscritos en este tema
                 */
                for (var i = 0, j = suscripciones[tema].length; i < j; i++) {
                    //Si el indice es exactamente igual al token que pasamos
                    if (suscripciones[tema][i] === token) {
                        /*
                        Con splice se elimina el objeto del array de suscripciones desde la posicion i y solo se elimina 1 objeto
                        */
                        suscripciones[tema].splice(i, 1);
                        return token;
                    }
                }

            }

        }

        //Retorno la funcion de desuscribir
        return this;

    };



})(observable)



```

Ejecutamos el ejemplo del patron observer

```javascript

Creamos los objetos que van a ser creados en el factory



//Pruebas
console.log('\nObserver pattern\n');

var testHandler = function(topicos, data) {
    console.log(topicos + ": " + data);
};


var socialMessage = function(topicos, data) {

    console.log(topicos);
    for (var twitts in data) {
        $('#social').append('<li><p>Idioma: ' + data[twitts].lang + '</p><p>Mensaje: ' + data[twitts].text + '</p><p>Link: ' + data[twitts].source + '</p></li>');
    }

};



var alertas = function(topicos, data) {
    console.log(topicos);
    console.log(data);
};


```

Para subscribirnos
========
```javascript

//Para subscribirnos
var testSubscription = observable.suscribir('ejemplo1', testHandler),
    noticias = {};


```
```javascript

noticias.notificasiones = function(topicos, data) {
    console.log(topicos);
    console.log(data.modelos());
};
noticias.publicar = observable.suscribir('Maria tiene hambre', noticias.notificasiones)


//Al completat una llamada ajax
var Twitter = observable.suscribir('Twitter', socialMessage)

```

Publicamos mensajes
========
```javascript

// Publicamos mensajes
/*
Cada vez que se haga una publicasion en un tema especifico se ejecuta el metodo de notificacion del objeto
*/
observable.publicar('ejemplo1', 'hola mundo');
observable.publicar('ejemplo1', ['test', 'a', 'b', 'c']);

observable.publicar('Drones con NodeJs', {
    titulo: 'Entregar paquetes',
    modelos: ['KTB', 'CTBS']
});

observable.publicar('Maria tiene hambre', {
    titulo: 'Darle de comer a Maria',
    modelos: function() {
        alert('Llamar a rostipollos');
    }
});


```

Para darnos de baja
========
```javascript


//para darnos de baja
observable.desuscribir(testSubscription);


//Llamada ajax con Observer
$(function() {

    $.ajax({
        url: 'http://dgweb.hol.es/twitter.php',
        async: true,
        type: 'GET',
        dataType: 'json',
        success: function(datos) {
            //Al completarse la llamada ajax ejecuto la publicasion de la notificasion
            observable.publicar('Twitter', datos);
        }
    });


});

```








Chainable pattern
====================

Patron de encadenamiento
La base de este patron esta en que cada metodo del objeto retorna "this", de esta manera se puede encadenar un metodo a otro

```javascript

var Chainable = function(valor) {
    this.nNumber = valor || 0;
};

Chainable.prototype.sumar = function(nNumber) {
    this.nNumber += nNumber;
    return this;
}

Chainable.prototype.multiplicar = function(nNumber) {
    this.nNumber *= nNumber;
    return this;
}

Chainable.prototype.resta = function(nNumber) {
    this.nNumber -= nNumber;
    return this;
}

Chainable.prototype.toString = function() {
    return this.nNumber.toString();
}

```

Ejemplo de como usar el script

```javascript
console.log('\nChainable pattern\n');

var numero = new Chainable(5);
numero.sumar(2).multiplicar(2).resta(1);
console.log(numero.toString());

```
