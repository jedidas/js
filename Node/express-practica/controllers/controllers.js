
var controllers = function () {

    return {

        inicio: {
            index: function (request, response) {
                console.log('inicio');
                var data = {
                    title: 'Home'
                };
                response.render('index.jade', data);
            }
        },
        tienda: {
            index: function (request, response) {
                console.log('tienda');

                var productos = [
                    {titulo: 'Producto 1', img: 'product1.jpg', descripcion: 'The initial commit for Hapi was on August 5, 2011'},
                    {titulo: 'Producto 2', img: 'product2.jpg', descripcion: 'The initial commit for Hapi was on August 5, 2011'},
                    {titulo: 'Producto 3', img: 'product3.jpg', descripcion: 'The initial commit for Hapi was on August 5, 2011'},
                    {titulo: 'Producto 4', img: 'product4.jpg', descripcion: 'The initial commit for Hapi was on August 5, 2011'},
                    {titulo: 'Producto 5', img: 'product5.jpg', descripcion: 'The initial commit for Hapi was on August 5, 2011'},
                ];

                var data = {
                    title: 'Tienda',
                    productos: productos
                };
                response.render('tienda.jade', data);
            }
        }

    };

};

exports.controllers = controllers;