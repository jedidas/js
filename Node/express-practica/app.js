'use strict';
var express = require('express'),
    fs = require('fs'),
    App = express(),
    controllers = require('./controllers/controllers');
    controllers = controllers.controllers();
    console.log( controllers );

    App.use( express.static( __dirname + '/public' ) );
    App.get('/', controllers.inicio.index);
    App.get('/tienda', controllers.tienda.index);


    App.listen(3000, function () {
        console.log('Puerto node');
    });