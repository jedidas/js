'use strict';
//Cargo modulo HTTP
var http = require("http"),
	urls = require('url'),
	fs = require('fs'),
	puerto = 8888;


function AppNode (url, response) {

	var view = '',
		rutas = {
		'/': function () {
			view = 'layout.html';
			console.log("index.html");
		},
		'/somos': function () {
			view = 'somos.html';
			console.log("somos.html");
		},
		'/404': function () {
			view = '404.html';
			console.log("404.html");
		}
	};

	if(rutas.hasOwnProperty(url)){
		rutas[url]();
	}else{
		if(rutas.hasOwnProperty('/'+url)){
			rutas[url]();
		}else{
			rutas['/404']();
		}
	}

	fs.readFile('./views/'+view, function (error, data){
		response.write(data);
		response.end();
	});

	console.log(url);

}


//Inicio el server
http.createServer(function (request, response) {

	//Encabezado de la respuesta
	response.writeHead(200, {'Content-Type': 'text/html'});

	var url = urls.parse( request.url );
	AppNode( url.pathname, response );

}).listen(puerto, 'localhost', function(){

});


