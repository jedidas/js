'use strict';

var server = require("./server"),
	router = require("./router"),
	requestHandlers = require("./requestHandlers");;


//Rutas
var handle = {}
handle["/"] = requestHandlers.iniciar;
handle["/iniciar"] = requestHandlers.iniciar;
handle["/subir"] = requestHandlers.subir;

server.iniciar(router.router, handle);
