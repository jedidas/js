'use strict';

//Cargo modulo HTTP
var http = require("http"),
	url = require("url");;

function iniciar (route, handle) {

	function onRequest(request, response) {

		//ruta
		var pathname = url.parse(request.url).pathname;
		//Paso la ruta al ruteador
		route(handle, pathname);
		//Encabezado de la respuesta
		response.writeHead(200, {'Content-Type': 'text/html'})
		//Mensaje a mostrar
		response.write('Hola Yo');
		response.end();

	}

	//Inicio el server
	http.createServer(onRequest).listen(8888);

}

exports.iniciar = iniciar;